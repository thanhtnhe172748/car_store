﻿using Microsoft.Extensions.Primitives;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Auto99.Util
{
    public class Security
    {
        private static readonly string LOWERCASE_CHARACTERS = "abcdefghijklmnopqrstuvwxyz";
        private static readonly string UPPERCASE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private static readonly string DIGITS = "0123456789";
        private static readonly string SPECIAL_CHARACTERS = "!@#$%^&*()_+-=[]{}|;:'\",.<>?";

        //kiem tra mat khau voi chuoi ma hoa
        public bool Verify(string inputPassword, string hashPassword)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(inputPassword);
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                sb.Append(b.ToString("X2"));
            }
            return hashPassword.Equals(sb.ToString(), StringComparison.OrdinalIgnoreCase);
        }

        //ma hoa mat khau
        public string MD5Hash(string password)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(password);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                foreach (byte b in hashBytes)
                {
                    sb.Append(b.ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public string GenerateRandomString()
        {
            Random random = new Random();
            StringBuilder password = new StringBuilder();

            // Thêm một ký tự viết hoa và một ký tự đặc biệt
            password.Append(UPPERCASE_CHARACTERS[random.Next(UPPERCASE_CHARACTERS.Length)]);
            password.Append(SPECIAL_CHARACTERS[random.Next(SPECIAL_CHARACTERS.Length)]);

            // Thêm các ký tự ngẫu nhiên cho đến khi đạt được tối thiểu 8 ký tự
            while (password.Length < 8)
            {
                string characterSet = random.Next(2) == 0 ? LOWERCASE_CHARACTERS : DIGITS;
                password.Append(characterSet[random.Next(characterSet.Length)]);
            }

            // Trộn lẫn chuỗi
            string randomizedPassword = ShuffleString(password.ToString());

            return randomizedPassword;
        }

        public string CreateOTP()
        {
            Random random = new Random();
            StringBuilder otp = new StringBuilder();

            while (otp.Length < 6)
            {
                otp.Append(DIGITS[random.Next(DIGITS.Length)]);
            }
            return otp.ToString();
        }

        // su dung de tron lan chuoi
        public string ShuffleString(string input)
        {
            Random random = new Random();
            char[] charArray = input.ToCharArray();
            for (int i = charArray.Length - 1; i > 0; i--)
            {
                int index = random.Next(i + 1);
                char temp = charArray[index];
                charArray[index] = charArray[i];
                charArray[i] = temp;
            }
            return new string(charArray);
        }

        public string GetRandomAlphaNumeric(int len)
        {
            String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder(len);
            for (int i = 0; i < len; i++)
            {
                int index = rnd.Next(chars.Length);
                sb.Append(chars[index]);
            }
            return sb.ToString();
        }
    }
}
