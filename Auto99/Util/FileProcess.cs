﻿using System.Text;

namespace Auto99.Util
{
    public class FileProcess
    {
        //Ảnh chính cho web gồm(ảnh sản phẩm, ảnh blog, ảnh slider)
        public readonly string LOADIMAGETOFILEIMG = "wwwroot/img/";
        public readonly string LOADIMAGETOFILEIMGBLOG = "wwwroot/img/Blog/";
        public readonly string LOADIMAGETOFILEIMGCAR = "wwwroot/img/Xe/";
        public readonly string LOADIMAGETOFILEIMGBRAND = "wwwroot/img/Brand/";
        public readonly string LOADIMAGETOFILEIMGEMPLOYEE = "wwwroot/img/EmployeeIMG/";
        //Ảnh linh tinh, ảnh người dùng( sử dụng trong quản lý người dùng, profile...)
        public readonly string LOADIMAGETOFILELOADIMG = "wwwroot/LoadImg/";

        public StringBuilder GetRelativePath(string filePath)
        {
            string appPath = Path.GetFullPath(filePath);
            //string link = ConvertPath(Path.Combine(appPath, filePath));
            StringBuilder linkPath = EditLocation(appPath);
            return linkPath;
        }

        public StringBuilder GetRelativePathFileBuild(string filePath)
        {
            string appPath = Path.GetFullPath(filePath);
            //string link = ConvertPath(Path.Combine(appPath, filePath));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(appPath);
            return stringBuilder;
        }

        public string ConvertPath(string path)
        {
            return path.Replace("\\", "/");
        }

        public StringBuilder EditLocation(string path)
        {
            string[] splits = path.Split(new[] { "build/" }, StringSplitOptions.None);
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string item in splits)
            {
                stringBuilder.Append(item);
            }
            return stringBuilder;
        }


        public void deleteImage(string URL)
        {
            File.Delete(URL);
        }

        //public void readImageLoaded(string absolutePath, string ImageUpload, IFormFile partFileImageUpload){
        //    OutputStream out = null;
        //    InputStream filecontent = null;
        //    File fileImageUpload = new File(absolutePath + ImageUpload);
        //    out = new FileOutputStream(fileImageUpload);
        //    filecontent = partFileImageUpload.getInputStream();
        //    int readImageUpload = 0;
        //    readonly byte[] bytesImageUpload = new byte[2048];
        //    while ((readImageUpload = filecontent.read(bytesImageUpload)) != -1) {
        //        out.write(bytesImageUpload, 0, readImageUpload);
        //    }
        //}
    }
}
