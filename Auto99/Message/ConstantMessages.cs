﻿namespace Auto99.Message
{
    public class ConstantMessages
    {
        public readonly string SUCCESS = "Thành công";
        public readonly string DUPALICATE = "Tài khoản đã tồn tại";
        public readonly string EMPTY = "Không được để trống";
        public readonly string PATTERN = "Yêu cầu nhập đúng định dạng";
        public readonly string DUPALICATEACCOUNT = "Tên tài khoản đã được sử dụng";
        public readonly string DUPALICATEEMAIL = "Email đã được sử dụng cho tài khoản khác";
        public readonly string DUPALICATEPHONE = "Số điện thoại đã được sử dụng cho tài khoản khác";
        public readonly string DUPALICATEIDNO = "Chứng minh nhân dân đã được sử dụng cho tài khoản khác";
        public readonly string CHECKIMAGEFORMAT = "Ảnh không đúng định dạng";
    }
}
