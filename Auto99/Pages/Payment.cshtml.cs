using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using System.Text.Encodings.Web;
using System.Text;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using ZaloPayDemo.ZaloPay.Crypto;
using ZaloPayDemo.ZaloPay;
using Auto99.Util;

namespace Auto99.Pages
{
    public class PaymentModel : PageModel
    {
        public string ResponseMessage { get; set; }
        public string PaymentUrl { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            string app_id = "2553";
            string key1 = "PcY4iZIKFCIdgZvA6ueMcMHHUbRLYjPL";
            string create_order_url = "https://sb-openapi.zalopay.vn/v2/create";

            Random rnd = new Random();
            var embed_data = new Dictionary<string, string>
                {
                    { "redirecturl", "https://localhost:7270/CallBack" }
                };
            var items = new[] { new { } };
            var param = new Dictionary<string, string>();
            var app_trans_id = rnd.Next(1000000); // Generate a random order's ID.

            param.Add("app_id", app_id);
            param.Add("app_user", "user123");
            param.Add("app_time", Utils.GetTimeStamp().ToString());
            param.Add("amount", "20000000");
            param.Add("app_trans_id", DateTime.Now.ToString("yyMMdd") + "_" + app_trans_id); // mã giao dich có định dạng yyMMdd_xxxx
            param.Add("embed_data", JsonConvert.SerializeObject(embed_data));
            param.Add("item", JsonConvert.SerializeObject(items));
            param.Add("description", "Lazada - Thanh toán đơn hàng #" + app_trans_id);
            //param.Add("bank_code", "zalopayapp");

            var data = app_id + "|" + param["app_trans_id"] + "|" + param["app_user"] + "|" + param["amount"] + "|"
                + param["app_time"] + "|" + param["embed_data"] + "|" + param["item"];
            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, data));

            var result = await HttpHelper.PostFormAsync(create_order_url, param);

            if (result.TryGetValue("order_url", out object paymentUrl))
            {
                return Redirect(paymentUrl.ToString());
            }
            return Redirect("/Homepage");
        }
    }
}
