using Auto99.Models;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Auto99.Pages
{
    public class HomePageModel : PageModel
    {
        private readonly IBlogRepository _blogRepository;
        private readonly ICarRepository _carRepository;

        public HomePageModel(ICarRepository carRepository, IBlogRepository blogRepository)
        {
            _carRepository = carRepository;
            _blogRepository = blogRepository;
        }

        public List<Blog> ListBlog { get; set; }
        public List<BlogCategory> ListBC { get; set; }
        public List<List<Blog>> AllBlogsByPI { get; set; } = new List<List<Blog>>();
        public List<CarDesign> ListCD { get; set; }
        public List<List<Car>> AllCarsByDesign { get; set; } = new List<List<Car>>();
        public List<CarImg> AllCarImages { get; set; }
        public List<CarSubImg> AllCarSubImages { get; set; }
        public Dictionary<int?, int> ParentIDCountMap { get; set; } = new Dictionary<int?, int>();
        public List<EngineAndChassi> AllEngineAndChassis { get; set; }
        public List<GeneralInfoCar> AllGeneralInfoCars { get; set; }
        public List<Blog> AllBlogsByCategory { get; set; }
        public List<Blog> ListBlogID4 { get; set; }
        public void OnGet()
        {
            ListBlog = _blogRepository.GetAllBlog();
            ListCD = _carRepository.GetCarDesigns();
            ListBlogID4 = _blogRepository.GetListBlogID4(4);
            foreach (var cd in ListCD)
            {
                var car = _carRepository.GetCarsByDesignID(cd.DesignId);
                AllCarsByDesign.Add(car);
            }
            ListBC = _blogRepository.GetAllBlogCategory();
            AllCarImages = _carRepository.GetAllCarImgs();
            AllCarSubImages = _carRepository.GetAllCarSubImgs();
            ParentIDCountMap = new Dictionary<int?, int>();
            foreach (Blog blog in ListBlog)
            {
                if (blog.ParentId != null && blog.ParentId != 0)
                {
                    if (ParentIDCountMap.ContainsKey(blog.ParentId))
                    {
                        ParentIDCountMap[blog.ParentId] += 1;
                    }
                    else
                    {
                        ParentIDCountMap[blog.ParentId] = 1;
                    }
                }
            }
            foreach (var b in ListBlog)
            {
                var blog = _blogRepository.GetBlogByParentID(b.ParentId);
                AllBlogsByPI.Add(blog);
            }
            AllEngineAndChassis = _carRepository.GetEngineAndChassis();
            AllGeneralInfoCars = _carRepository.GetGeneralInfoCars();
            List<Blog> listb = _blogRepository.GetFooter();
            HttpContext.Session.SetObject("Footer", listb);
        }
    }
}
