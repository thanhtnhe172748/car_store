using Auto99.Models;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages
{
    public class BlogDetailModel : PageModel
    {
        private readonly IBlogRepository _blogRepository;
        public BlogDetailModel(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        public int BlogID { get; set; }
        public Blog Blog { get; set; }
        public List<Blog> ListAds { get; set; }
        public List<Blog> ListTTaKM { get; set; }
        public List<Blog> AdList { get; set; }
        public void OnGet(int Id)
        {
            BlogID = Id;
            Blog = _blogRepository.getBlogbyBlogId(BlogID); // Fetch Blog details

            if (Blog != null)
            {
                //FormattedDate = Blog.CreatedOn.ToString("dd-MM-yyyy HH:mm:ss"); // Format date as needed

                ListAds = _blogRepository.GetListAds(); // Fetch list of ads
                ListTTaKM = _blogRepository.GetAllTTaKm(); // Fetch list of TTaKM
                AdList = new List<Blog>();

                foreach (var subCategory in ListTTaKM)
                {
                    var ad = _blogRepository.GetAd(subCategory.ParentId);
                    if (ad != null)
                    {
                        AdList.Add(ad);
                    }
                }
            }
        }
    }
}
