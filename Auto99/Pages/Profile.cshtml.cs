using Auto99.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages
{
    public class ProfileModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        public ProfileModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public Client client { get; set; }
        public IActionResult OnGet()
        {
            var userClaims = User.Claims;
            var userIdClaim = userClaims.FirstOrDefault(c => c.Type == "Id");
            var userId = userIdClaim?.Value;
            if (userId == null)
            {
                return RedirectToPage("/Login");
            }
            client = _context.Clients.FirstOrDefault(x => x.ClientId == int.Parse(userId));
            return Page();
        }

        public IActionResult OnPost(IFormCollection result)
        {
            int userid = int.Parse(result["userid"]);
            string clientName = result["clientName"];
            string email = result["email"];
            string phonenumber = result["phonenumber"];
            string noid = result["noid"];
            string gender = result["gender"];
            DateTime dob = DateTime.Parse(result["dob"]);

            Client user = _context.Clients.FirstOrDefault(c => c.ClientId == userid);
            if(!user.Email.Equals(email) && _context.Clients.FirstOrDefault(c => c.Email == email) != null)
            {
                ViewData["messError"] = "Duplicate email!";
                client = _context.Clients.FirstOrDefault(x => x.ClientId == userid);
                return Page();
            }
            user.ClientName= clientName;
            user.Email= email;
            user.PhoneNumber= phonenumber;
            user.NoId= noid;
            user.Gender = !string.IsNullOrEmpty(gender) ? (gender.Equals("1") ? true : false) : null;
            user.Dob = dob;

            ClientAccount clientAccount= _context.ClientAccounts.FirstOrDefault(c => c.AccId == userid);
            clientAccount.Email= email;
            _context.SaveChanges();
            client = _context.Clients.FirstOrDefault(x => x.ClientId == userid);
            ViewData["messSuc"] = "Update profile successfully!";
            return Page();
        }
    }
}
