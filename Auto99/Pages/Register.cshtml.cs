using Auto99.Models;
using Auto99.Repositories;
using Auto99.Util;
using Humanizer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Auto99.Pages
{
    public class RegisterModel : PageModel
    {
        private readonly IAccountRepository _accRepository;

        public RegisterModel(IAccountRepository accRepository)
        {
            _accRepository = accRepository;
        }
        [TempData]
        public string SuccessMessage { get; set; }
        [BindProperty]
        public UserInput Input { get; set; }

        public class UserInput
        {
            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            [Required]
            public string UserName { get; set; }

            [Required]
            [DataType(DataType.Date)]
            public DateTime DateOfBirth { get; set; }

            [Required]
            public string Gender { get; set; }
        }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            Security se = new Security();
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (_accRepository.CheckAccountExist(Input.Email) == true)
            {
                ModelState.AddModelError(string.Empty, "Email is already existed.");
                return Page();
            }
            bool gender;
            if (Input.Gender.Equals("Male"))
            {
                gender = true;
            }
            else
            {
                gender = false;
            }
            var accPro = new Client
            {
                ClientName = Input.UserName,
                Email = Input.Email,
                Dob = Input.DateOfBirth,
                Gender = gender,
            };                      
            _accRepository.AddAccountProfile(accPro);
            var account = new ClientAccount
            {
                AccId = _accRepository.GetClientIdByEmail(Input.Email),
                Email = Input.Email,
                Pass = se.MD5Hash(Input.Password),
                Status = true
            };
            _accRepository.AddAccount(account);
            return RedirectToPage("/LoginClient");
        }
    }
}
