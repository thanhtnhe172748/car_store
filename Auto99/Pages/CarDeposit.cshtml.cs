using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.DotNet.Scaffolding.Shared.CodeModifier.CodeChange;
using System.Security.Cryptography;
using System.Text;
using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using ZaloPayDemo.ZaloPay.Crypto;
using ZaloPayDemo.ZaloPay;
using Auto99.Util;
using System.Collections.Generic;

namespace Auto99.Pages
{
    public class CarDepositModel : PageModel
    {
        private readonly database_SWP391_Auto99_version35Context _context;
        private readonly ICarRepository _carRepository;
        private readonly IFeeRepository _feeRepository;
        public CarDepositModel(ICarRepository carRepository, IFeeRepository feeRepository, database_SWP391_Auto99_version35Context context)
        {
            _carRepository = carRepository;
            _feeRepository = feeRepository;
            _context = context;
        }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string Phone { get; set; }
        [BindProperty]
        public string ID { get; set; }
        [BindProperty]
        public string Email { get; set; }
        [BindProperty]
        public int CarId { get; set; }
        [BindProperty]
        public double Price { get; set; }
        public Car Car { get; set; }
        public EngineAndChassi EngineAndChassis { get; set; }
        public List<CarImg> AllCarImages { get; set; }
        public List<CarSubImg> AllCarSubImages { get; set; }
        public GeneralInfoCar GeneralInfoCars { get; set; }
        public List<CarDesign> ListCD { get; set; }
        public CarSubImg SubImg { get; set; }
        public Client client { get; set; }
        public IActionResult OnGet(int Id)
        {
            var userClaims = User.Claims;
            var userIdClaim = userClaims.FirstOrDefault(c => c.Type == "Id");
            var userId = userIdClaim?.Value;
            if (userId == null)
            {
                return RedirectToPage("/LoginClient");
            }
            client = _context.Clients.FirstOrDefault(x => x.ClientId == int.Parse(userId));
            Car = _carRepository.GetCarById(Id);
            SubImg = _carRepository.GetCarSubImg(Id);
            //var fees = _feeRepository.GetListFee();
            Price = Car.Price;
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(IFormCollection result)
        {
            Voucher v = new Voucher();
            int carId = int.Parse(result["carID"]);
            int userId = int.Parse(result["userId"]);
            client = _context.Clients.FirstOrDefault(x => x.ClientId == userId);
            Car = _carRepository.GetCarById(carId);
            SubImg = _carRepository.GetCarSubImg(carId);
            Price = Car.Price;
            string searchVoucher = result["searchVoucher"];
            string submitOrder = result["submitOrder"];
            string voucher = result["voucher"];
            if (!string.IsNullOrEmpty(searchVoucher) || (!string.IsNullOrEmpty(submitOrder) && !string.IsNullOrEmpty(voucher)))
            {
                v = _context.Vouchers.Where(x => x.VoucherCode.Equals(voucher)).FirstOrDefault();
                if (v == null) {
                    ViewData["voucherMessage"] = "Voucher không tồn tại!";
                    return Page();
                }
                else
                {
                    ViewData["voucher"] = voucher;
                    if (v.EndDate<=DateTime.Now)
                    {
                        ViewData["voucherMessage"] = "Voucher đã hết hạn!";
                        return Page();
                    }
                    if (v.MaxUsage <= v.UsedCount)
                    {
                        ViewData["voucherMessage"] = "Voucher đã hết số lượt sử dụng!";
                        return Page();
                    }
                    if (v.ObjectVoucherId !=1)
                    {
                        ViewData["voucherMessage"] = "Voucher không đúng loại!";
                        return Page();
                    }
                    if (string.IsNullOrEmpty(submitOrder))
                    {
                        string mesVou = "Voucher hợp lệ! Bạn được giảm số tiền đặt cọc: ";
                        mesVou += v.DiscountValue.Value.ToString("N0");
                        mesVou += v.DiscountType.Value ? "%" : "VNĐ";
                        ViewData["voucherMessage"] = mesVou;
                        return Page();
                    }
                }
            }

            Security security = new Security();
            double money = 20000000;
            if (!string.IsNullOrEmpty(voucher))
            {
                if (v.DiscountType.Value)
                {
                    money -= money * v.DiscountValue.Value / 100;
                }
                else
                {
                    money -= v.DiscountValue.Value;
                }
            }
            string orderCode = security.GetRandomAlphaNumeric(13);
            while (_context.CarOrders.FirstOrDefault(x => x.CarorderCode.Equals(orderCode)) != null)
            {
                orderCode = security.GetRandomAlphaNumeric(13);
            }

            Random rnd = new Random();
            var embed_data = new
            {
                redirecturl = ZaloConfig.redirecturl
            };

            var items = new[] { new { } };
            var param = new Dictionary<string, string>();
            var app_trans_id = rnd.Next(1000000); // Generate a random order's ID.

            string app_id = ZaloConfig.appid;
            string key1 = ZaloConfig.key1;
            string create_order_url = ZaloConfig.createOrderUrl;

            param.Add("app_id", app_id);
            param.Add("app_user", "user123");
            param.Add("app_time", Utils.GetTimeStamp().ToString());
            param.Add("amount", money.ToString());
            param.Add("app_trans_id", DateTime.Now.ToString("yyMMdd") + "_" + orderCode); // mã giao dich có định dạng yyMMdd_xxxx
            param.Add("embed_data", JsonConvert.SerializeObject(embed_data));
            param.Add("item", JsonConvert.SerializeObject(items));
            param.Add("description", "Thanh toán đơn hàng #" + orderCode);
            //param.Add("bank_code", "zalopayapp");

            var data = app_id + "|" + param["app_trans_id"] + "|" + param["app_user"] + "|" + param["amount"] + "|"
                + param["app_time"] + "|" + param["embed_data"] + "|" + param["item"];
            param.Add("mac", HmacHelper.Compute(ZaloPayHMAC.HMACSHA256, key1, data));

            var oderURL = await HttpHelper.PostFormAsync(create_order_url, param);
            CarOrder carOrder =new CarOrder();
            if (oderURL.TryGetValue("order_url", out object paymentUrl))
            {
                carOrder.CarorderCode = orderCode;
                carOrder.ClientId = userId;
                carOrder.CarId = carId;
                carOrder.VoucherId= !string.IsNullOrEmpty(voucher)?v.VoucherId:null;
                carOrder.OrderValue = Car.Price;
                carOrder.PaymentRequired = Car.Price-20000000;
                carOrder.Paid = money;
                HttpContext.Session.SetObject("carOrder", carOrder);
                return Redirect(paymentUrl.ToString());
            }
            return Redirect("/Homepage");

            //_carRepository.AddOrder(null, clientId, CarId, true, totalPrice, totalPrice, totalPrice, createdOn, createdOn, status);

            
        }
       

        
    }
}
