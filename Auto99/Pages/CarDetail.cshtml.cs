using Auto99.Models;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages
{
    public class CarDetailModel : PageModel
    {
        private readonly ICarRepository _carRepository;
        public CarDetailModel(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }
        public Car Car { get; set; }
        public EngineAndChassi EngineAndChassis { get; set; }
        public List<CarImg> AllCarImages { get; set; }
        public List<CarSubImg> AllCarSubImages { get; set; }
        public GeneralInfoCar GeneralInfoCars { get; set; }
        public List<CarDesign> ListCD { get; set; }
        public void OnGet(int Id)
        {
            Car = _carRepository.GetCarById(Id);
            EngineAndChassis = _carRepository.GetEngineByCarId(Id);
            AllCarImages = _carRepository.GetAllCarImgs();
            AllCarSubImages = _carRepository.GetAllCarSubImgs();
            GeneralInfoCars = _carRepository.GetGeneralInfoCarByCarId(Id);
            ListCD = _carRepository.GetCarDesigns();
        }
    }
}
