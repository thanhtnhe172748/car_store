using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Security.Claims;
using Auto99.Services;
using Auto99.Repositories;
using Auto99.Models;
using Auto99.Util;

namespace Auto99.Pages
{
    public class LoginModel : PageModel
    {
        private readonly IAccountRepository _accRepository;

        public LoginModel(IAccountRepository accRepository)
        {
            _accRepository = accRepository;
        }
        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string Password { get; set; }

        [BindProperty]
        public bool RememberMe { get; set; }

        public string ErrorMessage { get; set; }
        public void OnGet()
        {
        }
        public async Task<IActionResult> OnPostAsync()
        {
            Security se = new Security();
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var isValidUser = _accRepository.GetAccount(Email, se.MD5Hash(Password));

            if(isValidUser != null)
            {
                var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, isValidUser.Email),
                new Claim("Id", isValidUser.AccId.ToString())
            };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToPage("/HomePage");
            }

            ErrorMessage = "Invalid login attempt.";
            return Page();
        }
    }
}
