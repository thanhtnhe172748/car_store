using Auto99.Models;
using Auto99.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Auto99.Pages
{
    public class PurchaseHistoryModel : PageModel
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public PurchaseHistoryModel(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }
        public List<CarOrder> CarOrders { get; set; }

        public void OnGet(int Id)
        {
            CarOrders =  _dbContext.CarOrders.Where(order => order.ClientId == Id).OrderByDescending(x => x.CreatedOn).Include(x => x.Car).ToList();
        }
    }
}
