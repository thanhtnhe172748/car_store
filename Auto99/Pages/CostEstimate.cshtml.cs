﻿using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Globalization;

namespace Auto99.Pages
{
    public class CostEstimateModel : PageModel
    {
        private readonly ICarRepository _carRepository;
        private readonly IFeeRepository _feeRepository;
        private readonly ILocationRepository _locationRepository;

        public CarBrand Brand { get; set; }
        public CarDesign Design { get; set; }
        public List<PolicyFee> FeeList { get; set; }
        public List<Location> LocationList { get; set; }
        public Car Car { get; set; }
        public double Total { get; set; }

        public CostEstimateModel(ICarRepository carRepository, IFeeRepository feeRepository, ILocationRepository locationRepository)
        {
            _carRepository = carRepository;
            _feeRepository = feeRepository;
            _locationRepository = locationRepository;
        }

        public void OnGet(int Id)
        {
            Car = _carRepository.GetCarById(Id);
            Total = CalculateTotal(Car);
            Brand = _carRepository.GetBrandById(Car.BrandId);
            var i = _carRepository.GetGeneralInfoCarByCarId(Id);
            Design = _carRepository.GetDesignById(i.DesignId);
            FeeList = _feeRepository.GetListFee();
            LocationList = _locationRepository.getListLocation();
        }

        private double CalculateTotal(Car car)
        {
            var fees = _feeRepository.GetListFee();
            double total = car.Price + fees.Sum(f => f.Fee);
            return total;
        }
    }
}
