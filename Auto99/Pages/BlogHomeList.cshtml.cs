using Auto99.Models;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages
{
    public class BlogHomeListModel : PageModel
    {
        private readonly IBlogRepository _blogRepository;

        public BlogHomeListModel(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        public List<BlogCategory> ListBC { get; set; }
        public List<Blog> ListBlog { get; set; }
        public List<Blog> ListTTaKM { get; set; }
        public List<Blog> ListBlog1 { get; set; }
        public List<Blog> Footer { get; set; }
        public List<Blog> AllBlogs { get; set; }
        public Blog Blog { get; set; }
        public List<Blog> ListBlogSP { get; set; }
        public int CurrentIndex { get; set; }
        public int EndPage { get; set; }
        public Dictionary<int, int> ParentIDCountMap { get; set; }
        int Count { get; set; }

        public void OnGet(int? blogID, int? index)
        {
            // Get data from DAO or repository
            ListBC = _blogRepository.GetAllBlogCategory();
            ListBlog = _blogRepository.GetAllBlog();
            ListTTaKM = _blogRepository.GetAllTTaKm();
            ListBlog1 = _blogRepository.GetAllTrueBlogs();
            Footer = _blogRepository.GetFooter();

            ParentIDCountMap = new Dictionary<int, int>();
            foreach (var blog in ListBlog)
            {
                int parentID = blog.ParentId;
                if (parentID != 0)
                {
                    if (!ParentIDCountMap.ContainsKey(parentID))
                        ParentIDCountMap[parentID] = 0;
                    ParentIDCountMap[parentID]++;
                }
            }

            var parentIDs = ListTTaKM.Select(b => b.ParentId.ToString()).ToArray();
            var listParentID = parentIDs.ToList();
            AllBlogs = _blogRepository.GetListBlogTTaKM(index ?? 1, listParentID);

            if (blogID.HasValue)
            {
                Blog = _blogRepository.getBlogbyBlogId(blogID.Value);
                ListBlogSP = _blogRepository.GetAllSameParentIDBlogByBlogID(blogID.Value);
                Count = _blogRepository.GetTotalAllSameParentIDBlogByBlogID(blogID.Value);
                EndPage = Count / 6;
                if (Count % 6 != 0)
                    EndPage++;
            }
            else
            {
                Count = _blogRepository.GetTotalBlogTTaKM(listParentID);
                EndPage = Count / 6;
                if (Count % 6 != 0)
                    EndPage++;
            }

            CurrentIndex = index ?? 1;
        }
    }
}
