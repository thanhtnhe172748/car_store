using Auto99.Models;
using Auto99.Repositories;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;

namespace Auto99.Pages
{
    public class ChangePasswordModel : PageModel
    {
        private readonly IAccountRepository _accRepository;

        public ChangePasswordModel(IAccountRepository accRepository)
        {
            _accRepository = accRepository;
        }

        [BindProperty(SupportsGet = true)]
        public int ClientId { get; set; }

        [Required]
        [BindProperty]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [BindProperty]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public void OnGet(int id)
        {
            ClientId = id;
        }

        public IActionResult OnPost()
        {
            Security se = new Security();
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var clientAccount = _accRepository.GetClientAccountById(ClientId);

            if (clientAccount == null)
            {
                return NotFound();
            }

            clientAccount.Pass = se.MD5Hash(Password);
            _accRepository.UpdateUser(clientAccount);

            return RedirectToPage("/HomePage");
        }
    }
}
