using Auto99.Models;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;

namespace Auto99.Pages
{
    public class CarListModel : PageModel
    {
        private readonly ICarRepository _carRepository;
        public CarListModel(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public List<Car> CarList { get; set; }
        public List<CarBrand> BrandList { get; set; }
        public List<CarDesign> DesignList { get; set; }
        public List<GeneralInfoCar> GeneralInfoCarsList { get; set; }
        public List<EngineAndChassi> EngineAndChassisList { get; set; }
        public List<CarImg> AllCarImages { get; set; }
        public List<CarSubImg> AllCarSubImages { get; set; }

        public void OnGet()
        {
            BrandList = _carRepository.GetCarBrands();
            DesignList = _carRepository.GetCarDesigns();
            GeneralInfoCarsList = _carRepository.GetGeneralInfoCars();
            EngineAndChassisList = _carRepository.GetEngineAndChassis();
            CarList = _carRepository.GetAllCars();
            AllCarImages = _carRepository.GetAllCarImgs();
            AllCarSubImages = _carRepository.GetAllCarSubImgs();
        }
    }
}
