using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using ZaloPayDemo.ZaloPay.Crypto;

namespace Auto99.Pages
{
    public class CallbackModel : PageModel
    {
        private readonly database_SWP391_Auto99_version35Context _context;
        public CallbackModel( database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }

        public async Task<IActionResult> OnGet()
        {
            var data = Request.Query;
            var checksumData = data["appid"] + "|" + data["apptransid"] + "|" + data["pmcid"] + "|" +
                data["bankcode"] + "|" + data["amount"] + "|" + data["discountamount"] + "|" + data["status"];
            string bankCode = data["bankcode"];
            CarOrder carOrder = HttpContext.Session.GetObject<CarOrder>("carOrder");
            if (data["status"].Equals("1"))
            {
                if (bankCode.Equals("CC"))
                {
                    carOrder.PaymentType = false;
                }
                else
                {
                    carOrder.PaymentType = true;
                }
                carOrder.CreatedOn = DateTime.Now;
                carOrder.Status = "1";
                _context.CarOrders.Add(carOrder);
                Voucher voucher = _context.Vouchers.Where(x => x.VoucherId == carOrder.VoucherId).FirstOrDefault();
                if (voucher != null)
                {
                    int voucherused = voucher.UsedCount!=null?voucher.UsedCount.Value:0;
                    voucherused++;
                    voucher.UsedCount=voucherused;
                }
                await _context.SaveChangesAsync();

                Client customer = _context.Clients.Where(x => x.ClientId == carOrder.ClientId).FirstOrDefault();

                string to = customer.Email;
                string title = "[Auto99] Thông báo về mã đơn hàng";
                string content1 = @"
<table width='100%' style='font-size:14px'>
    <tbody>
        <tr>
            <td height='20'></td>
        </tr>
        <tr>
            <td width='3%'></td>
            <td align='left'>
                <p style='color:#0066b3;line-height:150%'>
                    <strong>Kính gửi Quý khách hàng,</strong>
                </p>
                <p style='line-height:150%'>Xin trân trọng cảm ơn Quý khách hàng đã sử dụng dịch vụ của chúng tôi.</p>
                <p style='line-height:150%'>
                    Đơn vị <span style='font-weight:bold'>Auto99</span> thông báo: <span style='font-weight:bold'></span>
                </p>
                <p style='line-height:150%'>
                    Hóa đơn của khách hàng có:<br>
                    - Mã hóa đơn: <span style='font-weight:bold'>" + carOrder.CarorderCode + @"</span><br>
                    - Tên khách hàng: <span style='font-weight:bold'>" + customer.ClientName + @"</span><br>
                    - Trạng thái: Đã xác nhận đặt cọc
                </p>
                <p style='line-height:150%'>
                    Để xem chi tiết hóa đơn, Quý khách hàng vui lòng truy cập địa chỉ<br>  
                    <a href='#' target='_blank'>#</a><br>
                </p>
                <p style='line-height:150%'>Trân trọng cảm ơn Quý khách và chúc Quý khách nhiều thành công khi sử dụng dịch vụ!</p>
            </td>
        </tr>
    </tbody>
</table>";
                MailProcess mailSend = new MailProcess();
                await mailSend.sendMail(to, title, content1);
                HttpContext.Session.Clear();
            }   
            return Redirect("/HomePage");
        }
    }
}
