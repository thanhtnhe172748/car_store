using Auto99.Models;
using Auto99.Util;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace Auto99.Pages.Staff
{
    public class LoginModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string Password { get; set; }

        [BindProperty]
        public bool RememberMe { get; set; }

        public string ErrorMessage { get; set; }
        public LoginModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public void OnGet()
        {

        }

        public IActionResult OnPost(IFormCollection result) {
            Security se = new Security();
            string email = result["email"];
            string password = result["password"];
            if (!ModelState.IsValid)
            {
                return Page();
            }

            // Check login credentials (replace this with your own logic)
            var isValidUser = _context.Accounts.FirstOrDefault(a => a.Email == email && a.Password == se.MD5Hash(password));

            if (isValidUser == null)
            {
                ErrorMessage = "Invalid login attempt.";
                return Page();
            }
            var Role = _context.Roles.FirstOrDefault(x=>x.RoleId == isValidUser.RoleId);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, Email)
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.Session.SetString("StaffEmail", Email);
            HttpContext.Session.SetString("Role", Role.RoleName);
            HttpContext.User = claimsPrincipal;
            return RedirectToPage("/Staff/AdminHome");
        }
    }
}
