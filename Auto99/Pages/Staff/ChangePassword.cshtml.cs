using Auto99.Models;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages.Staff
{
    public class ChangePasswordModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        [BindProperty]
        public string Email { get; set; }

        [BindProperty]
        public string Password { get; set; }

        [BindProperty]
        public bool RememberMe { get; set; }

        public string ErrorMessage { get; set; }
        public ChangePasswordModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public void OnGet()
        {
        }
        public IActionResult OnPost(IFormCollection result)
        {
            Security se = new Security();
            string email = result["email"];
            string password = result["password"];
            string newP = result["newP"];
            string confirm = result["confirm"];
            Account isValidUser = _context.Accounts.FirstOrDefault(a => a.Email == email && a.Password == se.MD5Hash(password));

            if (isValidUser == null)
            {
                ErrorMessage = "Invalid email or password";
                return Page();
            }

            if (!newP.Equals(confirm))
            {
                ErrorMessage = "New password and confirm password do not match";
                return Page();
            }
            isValidUser.Password = se.MD5Hash(confirm);
            _context.SaveChanges();
            HttpContext.Session.SetString("StaffEmail", email);
            var Role = _context.Roles.FirstOrDefault(x => x.RoleId == isValidUser.RoleId);
            HttpContext.Session.SetString("Role", Role.RoleName);
            return RedirectToPage("/Staff/AdminHome");

        }
    }
}
