﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Auto99.Models;
using Auto99.Services;
using Auto99.Repositories;
using Auto99.Message;
using NuGet.Packaging.Signing;
using Auto99.Util;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class EmployeeListModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;

        private readonly IEmployeeRepository _empRepository;

        public EmployeeListModel(IEmployeeRepository empRepository, database_SWP391_Auto99_version35Context context)
        {
            _empRepository = empRepository;
            _context = context;
        }
        public IList<EmployeeProfile> EmployeeProfile { get;set; } = default!;



        public List<EmployeeProfile> ListEmp { get; set; }

        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            //string numberEmployeeDisplay = result["numberEmployeeDisplay"];
            //string page = result["page"];
            string numberEmployeeDisplay = "";
            string page = "";
            if (string.IsNullOrEmpty(numberEmployeeDisplay))
            {
                numberEmployeeDisplay = "5";
            }
            if (string.IsNullOrEmpty(page))
            {
                page = "1";
            }

            //Service function
            int currentPage = int.Parse(page);
            
            int totalClient = 0;
            int endPage = 0;
            ListEmp = _empRepository.GetListEmployee("", "", "", int.Parse(page), int.Parse(numberEmployeeDisplay));
                totalClient = _empRepository.GetTotalEmployee("", "", "");
                endPage = totalClient / int.Parse(numberEmployeeDisplay);
                if (totalClient % int.Parse(numberEmployeeDisplay) != 0)
                {
                    endPage++;
                }


            ViewData["listRole"] = _empRepository.GetListRole();
            ViewData["search"] = "";
            ViewData["numberEmployeeDisplay"] = numberEmployeeDisplay;
            ViewData["currentPage"] = currentPage;
            ViewData["page"] = page;
            ViewData["endPage"] = endPage;
            ViewData["filter"] = "";
            return Page();
        }


        public IActionResult OnPost(IFormCollection result)
        {
            string numberEmployeeDisplay = result["numberEmployeeDisplay"];
            string page = result["page"];
            string search = result["search"];
            string action = result["action"];
            string filter = result["filter"];

            //------get data to edit or add employee---------------
            FileProcess lo = new FileProcess();
            ConstantMessages constanMessage = new ConstantMessages();
            //HttpSession session = request.getSession();
            string firstName = result["firstName"];
            string lastName = result["lastName"];
            string gender = result["gender"];
            string DOB = result["DOB"];
            string email = result["email"];
            string phoneNumber = result["phoneNumber"];
            string IDNo = result["IDNo"];
            string address = result["address"];
            string startDate = result["startDate"];
            IFormFile image = result.Files["img"];
            string imgName="";
            string roleId = result["roleId"];
            //Account acc = (Account)session.getAttribute("acc");
            //int modifiedBy = acc.getAccID();
            //Timestamp modifiedOn = new Timestamp(System.currentTimeMillis());
            string employeeID = "";
            //--------end get data------------------------------------------


            if (!string.IsNullOrEmpty(action) && !action.Equals("filter") && !action.Equals("search"))
            {
                //---------------Nếu edit employee mà ko upload ảnh mới
                if (image == null || string.IsNullOrEmpty(image.FileName))
                {
                    if (action.Equals("edit"))
                    {
                        //Case use don't upload new image
                        imgName = result["img1"];
                        employeeID = result["employeeID"];
                        string StaffEmail = HttpContext.Session.GetString("StaffEmail");
                        int modifiedBy = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
                        bool check = _empRepository.UpdateEmployeeById(int.Parse(employeeID), firstName, lastName, email, phoneNumber, DOB, imgName, int.Parse(gender), IDNo, address, startDate, modifiedBy, int.Parse(roleId));
                        if (check)
                        {
                            return RedirectToPage("/Staff/EmployeeList");
                        }
                        
                    }
                }
                //---------------end

                string allowedExtensions = ".jpg|.jpeg|.png|.gif";
                string fileExtension = Path.GetExtension(image.FileName).Trim();
                if (allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    employeeID = result["employeeID"];
                    if (employeeID == null || string.IsNullOrEmpty(employeeID))
                    {
                        int idNum = _empRepository.GetTotalEmployee("", "", "") + 1;
                        employeeID = idNum.ToString();
                    }
                    imgName = "ImgEmp_" + employeeID + fileExtension;
                    StringBuilder absolutePath = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGEMPLOYEE);
                    string path = absolutePath + employeeID;
                    DirectoryInfo directory = new DirectoryInfo(path);
                    if (!directory.Exists)
                    {
                        directory.Create();
                    }
                    path += "/";

                    //delete old image
                    string oldImage = result["img1"];
                    if (oldImage != null)
                    {
                        lo.deleteImage(path + oldImage);
                    }
                    //copy ảnh mới vào folder
                    string destFile = Path.Combine(path, imgName);
                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        image.CopyTo(stream);
                    }


                }
                else
                {
                    //request.setAttribute("notification", constanMessage.CHECKIMAGEFORMAT);
                    //request.getRequestDispatcher("./Staff/Notification.jsp").forward(request, response);
                }
                //end image process------------

                //---------------action add
                if (action.Equals("add"))
                {
                    //            response.getWriter().print("1");
                    //SendMail sendMail = new SendMail();
                    //TextEmail textEmail = new TextEmail();
                    Security security = new Security();
                    //int createdBy = acc.getAccID();
                    string accountName = result["accountName"];
                    string password = security.GenerateRandomString();
                    try
                    {
                        string validate = _empRepository.CheckInsertEmployee(accountName, email, phoneNumber, IDNo);
                        if (validate.Equals(constanMessage.SUCCESS))
                        {
                            MailProcess mailSend = new MailProcess();
                            string to = email;
                            string title = "[Auto99] Thông báo về tài khoản nhân viên";
                            string content1 = @"
<table width='100%' style='font-size:14px'>
    <tbody>
        <tr>
            <td height='20'></td>
        </tr>
        <tr>
            <td width='3%'></td>
            <td align='left'>
                <p style='line-height:150%'>Chúc mừng bạn đã trở thành nhân viên của AUTO99.</p>
                <p style='line-height:150%'>
                    Đơn vị <span style='font-weight:bold'>Auto99</span> thông báo: <span style='font-weight:bold'></span>
                </p>
                <p style='line-height:150%'>
                    Mật khẩu tài khoản của bạn: <span style='font-weight:bold'>" + password + @" </span>
                </p>
                <p style='line-height:150%'>
                    Đăng nhập ở đây<br>  
                    <a href='https://localhost:7270/Staff/Login' target='_blank'>Login</a><br>
                </p>
            </td>
        </tr>
    </tbody>
</table>";
                            mailSend.sendMail(to, title, content1);
                            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
                            int createdBy = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
                            bool check = _empRepository.InsertEmployee(firstName, lastName, email, phoneNumber, DOB, imgName, int.Parse(gender), IDNo, address, startDate, createdBy, accountName, security.MD5Hash(password), int.Parse(roleId));
                            return RedirectToPage("/Staff/EmployeeList");
                        }
                        else
                        {
                            ViewData["messError"] = validate;
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                //------end

                //------action edit
                if (action.Equals("edit"))
                {
                    //case user upload new image

                    int modifiedBy = 1;
                    bool check = _empRepository.UpdateEmployeeById(int.Parse(employeeID), firstName, lastName, email, phoneNumber, DOB, imgName, int.Parse(gender), IDNo, address, startDate, modifiedBy, int.Parse(roleId));
                    if (check)
                    {
                        OnGet();
                    }
                }
                //-----end

            }


            //validate request
            if (page == null || string.IsNullOrEmpty(page))
            {
                page = "1";
            }

            if (numberEmployeeDisplay == null || string.IsNullOrEmpty(numberEmployeeDisplay))
            {
                numberEmployeeDisplay = "5";
            }
            if (search == null)
            {
                search = "";
            }

            if (action == null)
            {
                action = "";
            }

            if (filter == null)
            {
                filter = "";
            }

            //Service function
            int currentPage = int.Parse(page);

            int totalClient = 0;
            int endPage = 0;
            ListEmp = _empRepository.GetListEmployee(action, filter, search, int.Parse(page), int.Parse(numberEmployeeDisplay));
            totalClient = _empRepository.GetTotalEmployee(action, filter, search);
            endPage = totalClient / int.Parse(numberEmployeeDisplay);
            if (totalClient % int.Parse(numberEmployeeDisplay) != 0)
            {
                endPage++;
            }


            ViewData["listRole"] = _empRepository.GetListRole();
            ViewData["search"] = search;
            ViewData["numberEmployeeDisplay"] = numberEmployeeDisplay;
            ViewData["currentPage"] = currentPage;
            ViewData["page"] = page;
            ViewData["endPage"] = endPage;
            ViewData["filter"] = filter;
            return Page();
            
        }
    }
}
