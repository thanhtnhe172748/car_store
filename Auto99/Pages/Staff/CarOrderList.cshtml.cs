using Auto99.Models;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Xml.Linq;

namespace Auto99.Pages.Staff
{
    public class CarOrderListModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        public CarOrderListModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public List<CarOrder> ListCarOrder { get; set; }
        public List<Client> ListCustomer { get; set; }
        public List<Car> ListCarName { get; set; }
        public List<Voucher> ListVoucher { get; set; }
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            string pageIndex = "1";
            string search = "";
            string status = "";
            string paymentType = "";

            string startDate = "";
            string endDate = "";

            string quantityShow = "10";
            int quantity = int.Parse(quantityShow);
            int currentIndex = int.Parse(pageIndex);
            ListCarOrder = _context.CarOrders.OrderByDescending(x=>x.CreatedOn).Take(quantity).ToList();
            ListCustomer = _context.Clients.ToList();
            ListCarName = _context.Cars.ToList();
            ListVoucher = _context.Vouchers.ToList();
            int count = _context.CarOrders.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"] = endPage;
            ViewData["currentIndex"] = currentIndex;
            ViewData["index"] = pageIndex;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["search"] = search;
            ViewData["status"] = status;
            ViewData["paymentType"] = paymentType;
            ViewData["startDate"] = startDate;
            ViewData["endDate"] = endDate;
            return Page();
        }

        public IActionResult OnPost(IFormCollection result)
        {
            string action = result["action"];
            ListCustomer = _context.Clients.ToList();
            MailProcess mailSend = new MailProcess();
            if (!string.IsNullOrEmpty(action) && action.Equals("UpdateCarOrder"))
            {
                string id = result["carorderID"];
                string statusSelect = result["statusSelect"];
                CarOrder co = _context.CarOrders.Where(x => x.CarorderId == int.Parse(id)).FirstOrDefault();
                co.Status = statusSelect;
                if (statusSelect.Equals("1"))
                {
                    Client customer = ListCustomer.Where(x => x.ClientId == co.ClientId).FirstOrDefault();
                    co.OrderDate = DateTime.Now;
                    string to = customer.Email;
                    string title = "[Auto99] Thông báo về mã đơn hàng";
                    string content1 = @"
<table width='100%' style='font-size:14px'>
    <tbody>
        <tr>
            <td height='20'></td>
        </tr>
        <tr>
            <td width='3%'></td>
            <td align='left'>
                <p style='color:#0066b3;line-height:150%'>
                    <strong>Kính gửi Quý khách hàng,</strong>
                </p>
                <p style='line-height:150%'>Xin trân trọng cảm ơn Quý khách hàng đã sử dụng dịch vụ của chúng tôi.</p>
                <p style='line-height:150%'>
                    Đơn vị <span style='font-weight:bold'>Auto99</span> thông báo: <span style='font-weight:bold'></span>
                </p>
                <p style='line-height:150%'>
                    Hóa đơn của khách hàng có:<br>
                    - Mã hóa đơn: <span style='font-weight:bold'>"+ co.CarorderCode + @"</span><br>
                    - Tên khách hàng: <span style='font-weight:bold'>"+ customer.ClientName + @"</span><br>
                    - Trạng thái: Đã xác nhận đặt cọc
                </p>
                <p style='line-height:150%'>
                    Để xem chi tiết hóa đơn, Quý khách hàng vui lòng truy cập địa chỉ<br>  
                    <a href='#' target='_blank'>#</a><br>
                </p>
                <p style='line-height:150%'>Trân trọng cảm ơn Quý khách và chúc Quý khách nhiều thành công khi sử dụng dịch vụ!</p>
            </td>
        </tr>
    </tbody>
</table>";
                    mailSend.sendMail(to, title, content1);
                }
                if (statusSelect.Equals("2"))
                {
                    Client customer = ListCustomer.Where(x => x.ClientId == co.ClientId).FirstOrDefault();
                    string to = customer.Email;
                    string title = "[Auto99] Thông báo về mã đơn hàng";
                    string content1 = @"
<table width='100%' style='font-size:14px'>
    <tbody>
        <tr>
            <td height='20'></td>
        </tr>
        <tr>
            <td width='3%'></td>
            <td align='left'>
                <p style='color:#0066b3;line-height:150%'>
                    <strong>Kính gửi Quý khách hàng,</strong>
                </p>
                <p style='line-height:150%'>Xin trân trọng cảm ơn Quý khách hàng đã sử dụng dịch vụ của chúng tôi.</p>
                <p style='line-height:150%'>
                    Đơn vị <span style='font-weight:bold'>Auto99</span> thông báo: <span style='font-weight:bold'></span>
                </p>
                <p style='line-height:150%'>
                    Hóa đơn của khách hàng có:<br>
                    - Mã hóa đơn: <span style='font-weight:bold'>" + co.CarorderCode + @"</span><br>
                    - Tên khách hàng: <span style='font-weight:bold'>" + customer.ClientName + @"</span><br>
                    - Trạng thái: Đã bị hủy
                </p>
                <p style='line-height:150%'>
                    Để xem chi tiết hóa đơn, Quý khách hàng vui lòng truy cập địa chỉ<br>  
                    <a href='#' target='_blank'>#</a><br>
                </p>
                <p style='line-height:150%'>Trân trọng cảm ơn Quý khách và chúc Quý khách nhiều thành công khi sử dụng dịch vụ!</p>
            </td>
        </tr>
    </tbody>
</table>";
                    mailSend.sendMail(to, title, content1);
                }
                co.ModifiedBy = 1;
                co.ModifiedOn = DateTime.Now;
                _context.SaveChanges();
            }

            string pageIndex = result["index"];
            if (string.IsNullOrEmpty(pageIndex))
            {
                pageIndex = "1";
            }

            string search = result["search"];
            if (string.IsNullOrEmpty(search))
            {
                search = "";
            }

            string status = result["status"];
            if (string.IsNullOrEmpty(status))
            {
                status = "";
            }

            string paymentType = result["paymentType"];
            if (string.IsNullOrEmpty(paymentType))
            {
                paymentType = "";
            }

            string startDate = result["startDate"];
            if (string.IsNullOrEmpty(startDate))
            {
                startDate = "";
            }
            string endDate = result["endDate"];
            if (string.IsNullOrEmpty(endDate))
            {
                endDate = "";
            }

            string quantityShow = result["quantity"];
            if (string.IsNullOrEmpty(quantityShow))
            {
                quantityShow = "10";
            }

            List<CarOrder> lstAll = _context.CarOrders.ToList();
            if (!string.IsNullOrEmpty(search))
            {
                lstAll = lstAll.Where(x => x.CarorderCode.Contains(search)).ToList();
            }
            if (!string.IsNullOrEmpty(paymentType))
            {
                lstAll = lstAll.Where(x => x.PaymentType == bool.Parse(paymentType)).ToList();
            }
            if (!string.IsNullOrEmpty(status))
            {
                lstAll = lstAll.Where(x => x.Status.Equals(status)).ToList();
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                lstAll = lstAll.Where(x => x.OrderDate >= DateTime.Parse(startDate).AddDays(-1)).ToList();
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                lstAll = lstAll.Where(x => x.OrderDate <= (DateTime.Parse(endDate).AddDays(1))).ToList();
            }

            int quantity = int.Parse(quantityShow);
            int currentIndex = int.Parse(pageIndex);
            ListCarOrder = lstAll.OrderByDescending(x => x.CreatedOn).Skip((currentIndex - 1) * quantity).Take(quantity).ToList();
            
            ListCarName = _context.Cars.ToList();
            ListVoucher = _context.Vouchers.ToList();
            int count = _context.CarOrders.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"] = endPage;
            ViewData["currentIndex"] = currentIndex;
            ViewData["index"] = pageIndex;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["search"] = search;
            ViewData["status"] = status;
            ViewData["paymentType"] = paymentType;
            ViewData["startDate"] = startDate;
            ViewData["endDate"] = endDate;
            return Page();
        }
    }
}
