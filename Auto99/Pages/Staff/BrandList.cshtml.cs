using Auto99.Models;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class BrandListModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        public BrandListModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public List<CarBrand> ListBrand { get; set; }
        public void OnGet()
        {
            string pageIndex = "1";
            int index = int.Parse(pageIndex);

            string name = "";

            string quantityShow = "5";
            int quantity = int.Parse(quantityShow);

            ListBrand = _context.CarBrands.Skip((index - 1) * quantity).Take(quantity).ToList();
            int count = _context.CarBrands.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"] = endPage;
            ViewData["index"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
        }

        public void OnPost(IFormCollection result)
        {
            FileProcess lo = new FileProcess();
            string brandName = result["brandName"];
            string brandId = result["brandID"];
            string action = result["action"];
            IFormFile image = result.Files["ImageUpload"];
            if (!string.IsNullOrEmpty(brandName))
            {
                if (action.Equals("AddBrand") && image!=null)
                {
                    string allowedExtensions = ".jpg|.jpeg|.png|.gif";
                    string fileExtension = Path.GetExtension(image.FileName).Trim();
                    string imgName = brandName + fileExtension;
                    StringBuilder absolutePath = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGBRAND);
                    string destFile = Path.Combine(absolutePath.ToString(), imgName);

                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        image.CopyTo(stream);
                    }
                    if (allowedExtensions.Contains(fileExtension.ToLower()))
                    {
                        CarBrand brand = new CarBrand();
                        brand.BrandName = brandName;
                        brand.BrandImg = imgName;
                        brand.CreatedBy = 1;
                        brand.CreatedOn = DateTime.Now;
                        _context.CarBrands.Add(brand);
                        _context.SaveChanges();
                    }
                }
                else

                if (action.Equals("UpdateBrand"))
                {
                    CarBrand brand = _context.CarBrands.Where(x => x.BrandId == int.Parse(brandId)).FirstOrDefault();
                    brand.BrandName = brandName;
                    brand.ModifiedBy = 1;
                    brand.ModifiedOn = DateTime.Now;

                    if(image != null)
                    {
                        string allowedExtensions = ".jpg|.jpeg|.png|.gif";
                        string fileExtension = Path.GetExtension(image.FileName).Trim();
                        if (allowedExtensions.Contains(fileExtension.ToLower()))
                        {
                            string imgName = brandName + fileExtension;
                            StringBuilder absolutePath = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGBRAND);
                            string destFile = Path.Combine(absolutePath.ToString(), imgName);

                            using (var stream = new FileStream(destFile, FileMode.Create))
                            {
                                image.CopyTo(stream);
                            }
                            brand.BrandImg = imgName;
                        }
                    }

                    _context.SaveChanges();
                }
            }

            string pageIndex = result["index"];
            if (string.IsNullOrEmpty(pageIndex))
            {
                pageIndex = "1";
            }
            int index = int.Parse(pageIndex);

            string name = result["name"];
            if (string.IsNullOrEmpty(name))
            {
                name = "";
            }

            string quantityShow = result["quantity"];
            if (string.IsNullOrEmpty(quantityShow))
            {
                quantityShow = "5";
            }
            int quantity = int.Parse(quantityShow);

            ListBrand = _context.CarBrands.Where(x => x.BrandName.Contains(name)).Skip((index - 1) * quantity).Take(quantity).ToList();
            int count = _context.CarBrands.Where(x => x.BrandName.Contains(name)).Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }


            ViewData["endPage"] = endPage;
            ViewData["index"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
        }
    }
}
