﻿using Auto99.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Auto99.Pages.Staff
{
    public class BlogListModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;
        public BlogListModel(Auto99.Models.database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }

        public IList<Blog> Blog { get; set; } = default!;
        [BindProperty]
        public string Quantity { get; set; }
        [BindProperty]
        public string Search { get; set; }
        [BindProperty]
        public string Parent { get; set; }
        [BindProperty]
        public string Status { get; set; }
        [BindProperty]
        public string StartDate { get; set; }
        [BindProperty]
        public string EndDate { get; set; }

        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }

        //SupportsGet giúp giữ lại data đc giao qua url vd: Staff/BlogList?pageNumber=2&pageSize=25
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = 10; // Adjust the page size as needed

        [BindProperty(SupportsGet = true)]
        public int PageNumber { get; set; } = 1;


        [BindProperty]
        public string Action { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            Status = string.Empty;
            Parent = string.Empty;
            await LoadDataAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormCollection f)
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            int employeeId = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
            if (Action != null && Action == "UpdateBlog")
            {
                int id = int.Parse(f["blogID"]);

                var findBlog = _context.Blogs.Find(id);
                if(findBlog != null)
                {
                    findBlog.Status = bool.Parse(Status);
                    findBlog.ModifiedBy = employeeId;
                    _context.SaveChanges();
                    //return Page();
                }
            }
            await LoadDataAsync();
            return Page();
        }
        private async Task LoadDataAsync()
        {
            var query = _context.Blogs.Include(e => e.BlogCategory).Include(x => x.CreatedByNavigation).Include(x => x.ModifiedByNavigation).AsQueryable();

            // Apply filters
            if (!string.IsNullOrEmpty(Search))
            {
                query = query.Where(b => b.Title.Contains(Search));
            }

            if (!string.IsNullOrEmpty(Status))
            {
                bool statusValue = bool.Parse(Status);
                query = query.Where(b => b.Status == statusValue);
            }
            else
            {
                Status = string.Empty;
            }

            if (!string.IsNullOrEmpty(Parent) && !Parent.Equals("0"))
            {
                int parentValue = int.Parse(Parent);
                query = query.Where(b => b.ParentId == parentValue);
            }
            else
            {
                Parent = string.Empty;
            }
            if (!string.IsNullOrEmpty(StartDate) && !string.IsNullOrEmpty(EndDate))
            {
                DateTime start = DateTime.ParseExact(StartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(EndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                query = query.Where(b => b.CreatedOn >= start && b.CreatedOn <= end);
            }
            var totalBlogs = await query.CountAsync();
            TotalPages = (int)Math.Ceiling(totalBlogs / (double)PageSize);

            Blog = await query
                .Skip((PageNumber - 1) * PageSize)
                .Take(PageSize)
                .ToListAsync();

            // Get categories
            var categoryQuery = from b in _context.Blogs
                                where (from blog in _context.Blogs
                                       group blog by blog.ParentId into g
                                       select g.Key).Contains(b.ParentId)
                                      && b.BlogCategoryId != null
                                select new { b.Title, b.ParentId };

            var lbsc = categoryQuery.ToList();
            ViewData["ListBSC"] = lbsc;
        }

    }
}
