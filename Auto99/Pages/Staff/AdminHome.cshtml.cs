using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages.Staff
{
    public class AdminHomeModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;
        public AdminHomeModel(Auto99.Models.database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if(string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            ViewData["carCount"]=_context.Cars.Count();
            ViewData["blogCount"] =_context.Blogs.Count();
            ViewData["clientCount"] =_context.Clients.Count();
            ViewData["accCount"] =_context.EmployeeProfiles.Count();
            return Page();
        }
    }
}
