using Auto99.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Auto99.Pages.Staff
{
    public class VoucherListModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context = new database_SWP391_Auto99_version35Context();
        public VoucherListModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public List<Voucher> ListVou { get; set; }
        public List<ObjectVoucher> ListObj { get; set; }
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            string pageIndex = "1";
            int index = int.Parse(pageIndex);

            string name = "";

            string quantityShow = "5";
            int quantity = int.Parse(quantityShow);

            ListVou = _context.Vouchers.Skip((index - 1) * quantity).Take(quantity).ToList();
            ListObj = _context.ObjectVouchers.ToList();
            int count = _context.Vouchers.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"] = endPage;
            ViewData["index"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
            ViewData["type"] = "";
            ViewData["status"] = "";
            return Page();
        }


        public IActionResult OnPost(IFormCollection result)
        {
            string action = result["action"];
            string voucherID = result["voucherID"];
            string vouchercode = result["vouchercode"];
            string status = result["status"];
            string statusSelect = result["statusSelect"];
            string description = result["description"];
            string objSelect = result["objSelect"];
            string discountType = result["discountType"];
            string discountValue = result["discountValue"];
            string maxUsage = result["maxUsage"];
            string startDate = result["startDate"];
            string endDate = result["endDate"];

            if (!string.IsNullOrEmpty(action) && action.Equals("UpdateVoucher"))
            {
                Voucher v = _context.Vouchers.Where(x => x.VoucherId == int.Parse(voucherID)).FirstOrDefault();
                v.Status = bool.Parse(statusSelect);
                v.ModifiedBy = 1;
                v.ModifiedOn=DateTime.Now;
                _context.SaveChanges();
            }
            if (!string.IsNullOrEmpty(action) && action.Equals("AddVoucher"))
            {
                string startDateCreate = result["startDateCreate"];
                string endDateCreate = result["endDateCreate"];
                Voucher v = new Voucher();
                v.VoucherCode = vouchercode;
                v.Description= description;
                v.ObjectVoucherId=int.Parse(objSelect);
                v.DiscountType=bool.Parse(discountType);
                v.Status = true;
                v.DiscountValue = double.Parse(discountValue);
                v.MaxUsage = int.Parse(maxUsage);
                v.StartDate = DateTime.Parse(startDateCreate);
                v.EndDate = DateTime.Parse(endDateCreate);
                v.CreatedBy = 1;
                v.CreatedOn = DateTime.Now;
                _context.Vouchers.Add(v);
                _context.SaveChanges();
            }

            string pageIndex = result["index"];
            if (string.IsNullOrEmpty(pageIndex))
            {
                pageIndex = "1";
            }
            int index = int.Parse(pageIndex);

            string name = result["name"];
            if (string.IsNullOrEmpty(name))
            {
                name = "";
            }

            string quantityShow = result["quantity"];
            if (string.IsNullOrEmpty(quantityShow))
            {
                quantityShow = "5";
            }
            int quantity = int.Parse(quantityShow);

            string obj = result["obj"];
            if (string.IsNullOrEmpty(obj))
            {
                obj = "0";
            }

            string type = result["type"];
            if (string.IsNullOrEmpty(type))
            {
                type = "";
            }
            if (string.IsNullOrEmpty(status))
            {
                status = "";
            }
            if (string.IsNullOrEmpty(startDate))
            {
                startDate = "";
            }
            if (string.IsNullOrEmpty(endDate))
            {
                endDate = "";
            }
            List<Voucher> lstAll = _context.Vouchers.ToList();
            if (!string.IsNullOrEmpty(name))
            {
                lstAll = lstAll.Where(x => x.VoucherCode.Contains(name)).ToList();
            }
            if (!obj.Equals("0"))
            {
                lstAll = lstAll.Where(x => x.ObjectVoucherId == int.Parse(obj)).ToList();
            }
            if (!string.IsNullOrEmpty(type))
            {
                lstAll = lstAll.Where(x => x.DiscountType == bool.Parse(type)).ToList();
            }
            if (!string.IsNullOrEmpty(status))
            {
                lstAll = lstAll.Where(x => x.Status == bool.Parse(status)).ToList();
            }
            if (!string.IsNullOrEmpty(startDate))
            {
                lstAll = lstAll.Where(x => x.StartDate>=DateTime.Parse(startDate)).ToList();
            }
            if (!string.IsNullOrEmpty(endDate))
            {
                lstAll = lstAll.Where(x => x.EndDate <= DateTime.Parse(endDate)).ToList();
            }


            ListVou = lstAll.Skip((index - 1) * quantity).Take(quantity).ToList();
            ListObj = _context.ObjectVouchers.ToList();
            int count = lstAll.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"] = endPage;
            ViewData["index"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
            ViewData["obj"] = int.Parse(obj);
            ViewData["type"] = type;
            ViewData["status"] = status;
            ViewData["endDate"] = endDate;
            ViewData["startDate"] = startDate;
            return Page();
        }

    }
}
