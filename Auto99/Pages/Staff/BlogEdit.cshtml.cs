﻿using Auto99.Models;
using Auto99.Services;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class BlogEditModel : PageModel
    {
        private readonly IBlogRepository _blogRepository;
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;

        public BlogEditModel( IBlogRepository blogRepository, Auto99.Models.database_SWP391_Auto99_version35Context context)
        {
            _blogRepository = blogRepository;
            _context = context;
        }

        [BindProperty]
        public string Parent { get; set; }
        [BindProperty]
        public Blog blog { get; set; }

        public async Task<IActionResult> OnGetAsync(string action, int blogid)
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            Parent = string.Empty;
            // Get categories
            var lbsc = _blogRepository.GetBlogCategoryByThanh();
            ViewData["ListBSC"] = lbsc;
            blog = _blogRepository.getBlogbyBlogId(blogid);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormCollection f)
        {
            int blogId = int.Parse(f["blogID"]);
            string title = f["title"];
            int parentID = int.Parse(f["parentID"]);
            string description = f["description"];
            string content = f["content"];
            IFormFile image = f.Files["ImageUpload"];
            string imgName = "";


            //if (image == null || string.IsNullOrEmpty(image.FileName))
            //{
                    //Case use don't upload new image
                    //imgName = result["img1"];
                    //employeeID = result["employeeID"];

            //if (check)
            //{
            //    return Redirect("/staff/bloglist");
            //}
            //else
            //{
            //    return Page();
            //}

            //}


            if (image != null && !string.IsNullOrEmpty(image.FileName))
            {
                //Lấy ảnh
                FileProcess lo = new FileProcess();
                string allowedExtensions = ".jpg|.jpeg|.png|.gif";
                string fileExtension = Path.GetExtension(image.FileName).Trim();
                if (allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    //employeeID = result["employeeID"];
                    //if (employeeID == null || string.IsNullOrEmpty(employeeID))
                    //{
                    //    int idNum = _empRepository.GetTotalEmployee("", "", "") + 1;
                    //    employeeID = idNum.ToString();
                    //}
                    string nextlastID = (_blogRepository.GetLastID() + 1).ToString();
                    imgName = "ImgBlog_" + nextlastID + fileExtension;
                    StringBuilder absolutePath = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGBLOG);
                    string path = absolutePath + "";
                    DirectoryInfo directory = new DirectoryInfo(path);
                    if (!directory.Exists)
                    {
                        directory.Create();
                    }
                    path += "/";

                    //delete old image
                    string oldImage = f["img1"];
                    if (oldImage != null && !string.IsNullOrEmpty(oldImage))
                    {
                        lo.deleteImage(path + oldImage);
                    }
                    //copy ảnh mới vào folder
                    string destFile = Path.Combine(path, imgName);
                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        image.CopyTo(stream);
                    }


                }
                else
                {
                    //request.setAttribute("notification", constanMessage.CHECKIMAGEFORMAT);
                    //request.getRequestDispatcher("./Staff/Notification.jsp").forward(request, response);
                }
                //end image process------------
            }
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            int employeeId = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
            int modifiedBy = employeeId;
            bool check = _blogRepository.UpdateBlogById(blogId, title, parentID, description, content, imgName, modifiedBy);

            return Redirect("/staff/bloglist");
        }
    }
}
