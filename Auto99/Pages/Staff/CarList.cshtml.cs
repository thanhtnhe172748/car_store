using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;

namespace Auto99.Pages.Staff
{
    public class CarListModel : PageModel
    {
        private readonly ICarRepository _carRepository;

        public CarListModel(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public List<CarDesign> DesignList { get; set; }
        public List<CarBrand> BrandList { get; set; }
        public List<StatusCategory> StatusList { get; set; }
        public List<CarStaffPage> CarList { get; set; }
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            string pageIndex = "1";
            int index = int.Parse(pageIndex);

            string name = "";

            string quantityShow = "5";
            int quantity = int.Parse(quantityShow);

            string brandID = "0";
            int brand = int.Parse(brandID);

            string designID = "0";
            int design = int.Parse(designID);

            string statusID = "0";
            int status = int.Parse(statusID);

            double MAXCarPrice = _carRepository.GetMaxPriceCar(name, brand, design, status);
            string minValue = "0";
            double minPrice = double.Parse(minValue);

            double maxPrice = MAXCarPrice;
            string maxValue = "";

            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();
            CarList = _carRepository.GetListCarAdmin(index, name, quantity, brand, design, status, minPrice, maxPrice);
            int count = _carRepository.GetTotalCarAdmin(name, brand, design, status, minPrice, maxPrice);
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }
            ViewData["endPage"] = endPage;
            ViewData["CarList"] = CarList;
            ViewData["pageIndex"] = index;
            ViewData["count"] = count;
            ViewData["quantityShow"] = quantity;
            ViewData["name"] = name;
            ViewData["minPrice"] = minPrice;
            ViewData["maxPrice"] = maxPrice;
            ViewData["MAXCarPrice"] = MAXCarPrice;

            ViewData["designID"] = design;
            ViewData["DesignList"]= DesignList;

            ViewData["brandID"] = brand;
            ViewData["BrandList"]= BrandList;

            ViewData["statusID"]= status;
            ViewData["StatusList"]= StatusList;
            return Page();
        }

        public IActionResult OnPost(IFormCollection result)
        {
            //--Nhan paramater update car simple
            string carID = result["carID"];
            string carName = result["carName"];
            string statuschangeID = result["statusChangeID"];
            string price = result["price"];
            string action = result["action"];
            int modifiedBy = 1;
            if (!string.IsNullOrEmpty(action) && action.Equals("update"))
            {
                _carRepository.UpdateCar(carName, price, statuschangeID, int.Parse(carID), modifiedBy);
            }
            
            //------------------------
            string pageIndex = result["pageIndex"];
            if (string.IsNullOrEmpty(pageIndex))
            {
                pageIndex = "1";
            }
            int index = int.Parse(pageIndex);

            string name = result["name"];
            if (string.IsNullOrEmpty(name))
            {
                name = "";
            }

            string quantityShow = result["quantityShow"];
            if (string.IsNullOrEmpty(quantityShow))
            {
                quantityShow = "5";
            }
            int quantity = int.Parse(quantityShow);

            string brandID = result["brandID"];
            if (string.IsNullOrEmpty(brandID))
            {
                brandID = "0";
            }
            int brand = int.Parse(brandID);

            string designID = result["designID"];
            if (string.IsNullOrEmpty(designID))
            {
                designID = "0";
            }
            int design = int.Parse(designID);
            string statusID = result["statusID"];
            if (string.IsNullOrEmpty(statusID))
            {
                statusID = "0";
            }
            int status = int.Parse(statusID);

            
            double MAXCarPrice = _carRepository.GetMaxPriceCar(name, brand, design, status);
            string minValue = result["minPrice"];
            minValue = Regex.Replace(minValue, "[^0-9]", "");
            if (string.IsNullOrEmpty(minValue))
            {
                minValue = "0";
            }
            double minPrice = double.Parse(minValue);

            double maxPrice;
            string maxValue = result["maxPrice"];
            maxValue = Regex.Replace(maxValue, "[^0-9]", "");
            if (string.IsNullOrEmpty(maxValue) || (!string.IsNullOrEmpty(maxValue) && double.Parse(maxValue)>MAXCarPrice))
            {
                maxPrice = MAXCarPrice;
            }
            else
            {
                maxPrice = double.Parse(maxValue);
            }

            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();
            CarList = _carRepository.GetListCarAdmin(index, name, quantity, brand, design, status, minPrice, maxPrice);
            int count = _carRepository.GetTotalCarAdmin(name, brand, design, status, minPrice, maxPrice);
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }
            ViewData["endPage"] = endPage;
            ViewData["CarList"] = CarList;
            ViewData["pageIndex"] = index;
            ViewData["count"] = count;
            ViewData["quantityShow"] = quantity;
            ViewData["name"] = name;
            ViewData["minPrice"] = minPrice;
            ViewData["maxPrice"] = maxPrice;
            ViewData["MAXCarPrice"] = MAXCarPrice;

            ViewData["designID"] = design;
            ViewData["DesignList"] = DesignList;

            ViewData["brandID"] = brand;
            ViewData["BrandList"] = BrandList;

            ViewData["statusID"] = status;
            ViewData["StatusList"] = StatusList;
            return Page();
        }
    }
}
