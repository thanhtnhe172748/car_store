using Auto99.Models;
using Auto99.Repositories;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Auto99.Pages.Staff
{
    public class LocationListModel : PageModel
    {
        private readonly ILocationRepository _locRepository;
        private readonly IEmployeeRepository _empRepository;

        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;


        public LocationListModel(Auto99.Models.database_SWP391_Auto99_version35Context context, ILocationRepository locRepository, IEmployeeRepository empRepository)
        {
                _context = context;
                _locRepository = locRepository;
            _empRepository = empRepository;
        }
        public List<Location> LocationList {get;set;}
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            string currentPage = "1";
            int index = int.Parse(currentPage);
            string name = "";
            string quantityShow = "5";
            int quantity = int.Parse(quantityShow);

            LocationList = _locRepository.getListLocation(index, quantity, name);
            int count = _locRepository.getTotalLocation(name);
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }
            List<EmployeeProfile> listEmp = _empRepository.GetListEmployee();
            ViewData["endPage"] = endPage;
            ViewData["listEmp"] = listEmp;
            ViewData["LocationList"] = LocationList;
            ViewData["_empRepository"] = _empRepository;
            ViewData["currentPage"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
            return Page();
        }


        public IActionResult OnPost(IFormCollection result)
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            int employeeId = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
            string currentPage = result["currentPage"];
            
            string name = result["name"];
            string quantityS = result["quantity"];
            

            if (string.IsNullOrEmpty(currentPage))
            {
                currentPage = "1";
            }
            int index = int.Parse(currentPage);
            if (string.IsNullOrEmpty(quantityS))
            {
                quantityS = "5";
            }
            int quantity = int.Parse(quantityS);

            //get parameter to add/update
            string locationName = result["locationName"];
            string preRegFee = result["preRegFee"];
            string regFee = result["regFee"];
            string action = result["action"];
            //-----------------
            
            if ("delete".Equals(action))
            {
                List<Location> lstAll = _locRepository.getListLocation();
                foreach(Location i in lstAll)
                {
                    string checke= result["checkedLo" + i.LocationId];
                    if (!string.IsNullOrEmpty(checke) && checke.Equals("checkedLo" + i.LocationId))
                    {
                        _locRepository.deleteLocation(i.LocationId);
                    }
                }
                return Redirect("/Staff/LocationList");
            }

            if ("AddLocation".Equals(action))
            {
                int createBy = employeeId;
                _locRepository.addLocation(locationName, preRegFee, regFee,createBy);
                return Redirect("/Staff/LocationList");
            }
            if ("UpdateLocation".Equals(action))
            {
                string LocationID = result["locationID"];
                if (LocationID == null)
                {
                    LocationID = "1";
                }
                int modifiBy = employeeId;
                _locRepository.updateLocation(locationName, preRegFee, regFee, modifiBy,int.Parse(LocationID));
                OnGet();
                return Redirect("/Staff/LocationList");
            }

            LocationList = _locRepository.getListLocation(index, quantity, name);

            int count = _locRepository.getTotalLocation(name);
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }
            List<EmployeeProfile> listEmp = _empRepository.GetListEmployee();
            ViewData["endPage"] = endPage;
            ViewData["listEmp"] = listEmp;
            ViewData["LocationList"] = LocationList;
            ViewData["_empRepository"] = _empRepository;
            ViewData["currentPage"] = int.Parse(currentPage);
            ViewData["quantity"] = quantityS;
            ViewData["name"] = name;

            foreach (Location i in LocationList)
            {
                string checke = result["checkedLo" + i.LocationId];
                if (!string.IsNullOrEmpty(checke) && checke.Equals("checkedLo" + i.LocationId))
                {
                    ViewData["checkedLo" + i.LocationId] = true;
                }
            }

            return Page();
        }
    }
}
