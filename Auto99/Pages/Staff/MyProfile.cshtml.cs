﻿using Auto99.Models;
using Auto99.Repositories;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class MyProfileModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;

        public MyProfileModel(Auto99.Models.database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }

        [BindProperty]
        public EmployeeProfile empP { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            var query = from ep in _context.EmployeeProfiles
                        join a in _context.Accounts on ep.EmployeeId equals a.AccId
                        join r in _context.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName),
                            Employee = a
                        };


            empP = query.Where(x => x.Email.Equals(StaffEmail)).FirstOrDefault();
            return Page();
        }
        public async Task<IActionResult> OnPostAsync(IFormCollection f)
        {

            IFormFile image = f.Files["ImageUpload"];
            string imgName = empP.Img;
            if (image != null)
            {
                //Lấy ảnh
                FileProcess lo = new FileProcess();
                string allowedExtensions = ".jpg|.jpeg|.png|.gif";
                string fileExtension = Path.GetExtension(image.FileName).Trim();
                if (allowedExtensions.Contains(fileExtension.ToLower()))
                {

                    // Generate a unique folder name based on a timestamp or some other identifier.
                    string EmpIMGFolder = empP.EmployeeId.ToString();

                    imgName = image.FileName;
                    StringBuilder absolutePathDirectory = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGEMPLOYEE);
                    string path = absolutePathDirectory + "";
                    DirectoryInfo directory = new DirectoryInfo(absolutePathDirectory + EmpIMGFolder);
                    if (!directory.Exists)
                    {
                        directory.Create();
                    }
                    //path += "/";
                    //DiskFileItemFactory factory = new DiskFileItemFactory();
                    //ServletContext servletContext = this.getServletConfig().getServletContext();
                    //File repository = (File)servletContext.getAttribute("javax.servlet.context.tempdir");
                    //factory.setRepository(repository);
                    //ServletFileUpload upload = new ServletFileUpload(factory);
                    //upload.setHeaderEncoding("UTF-8");
                    //lo.readImageLoaded(path, img, partFileImg);

                    //delete old image
                    //string oldImage = f["img1"];
                    //if (oldImage != null)
                    //{
                    //    lo.deleteImage(path + oldImage);
                    //}
                    //copy ảnh mới vào folder
                    string destFile = Path.Combine(directory.ToString(), imgName);
                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        image.CopyTo(stream);
                    }


                }
                else
                {
                    //request.setAttribute("notification", constanMessage.CHECKIMAGEFORMAT);
                    //request.getRequestDispatcher("./Staff/Notification.jsp").forward(request, response);
                }
            }


            try
            {
                var configuration = new ConfigurationBuilder()
                                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                    .AddJsonFile("appsettings.json")
                                    .Build();

                // Retrieve the connection string
                string connectionString = configuration.GetConnectionString("DbString");

                string sql = @"UPDATE EmployeeProfile
                       SET firstName = @FirstName,
                           lastName = @LastName,
                           DOB = @DOB,
                           phoneNumber = @PhoneNumber,
                           img = @Img,
                           gender = @Gender,
                           IDNo = @IDNo,
                           [address] = @Address,
                           modifiedBy = @ModifiedBy,
                           modifiedOn = @ModifiedOn
                       WHERE employeeID = @EmployeeID";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(sql, connection);

                    // Add parameters
                    command.Parameters.AddWithValue("@FirstName", empP.FirstName);
                    command.Parameters.AddWithValue("@LastName", empP.LastName);
                    command.Parameters.AddWithValue("@DOB", empP.Dob);
                    command.Parameters.AddWithValue("@PhoneNumber", empP.PhoneNumber);
                    command.Parameters.AddWithValue("@Img", imgName); // or the image data
                    command.Parameters.AddWithValue("@Gender", empP.Gender);
                    command.Parameters.AddWithValue("@IDNo", empP.Idno);
                    command.Parameters.AddWithValue("@Address", empP.Address);
                    command.Parameters.AddWithValue("@ModifiedBy", empP.EmployeeId);
                    command.Parameters.AddWithValue("@ModifiedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@EmployeeID", empP.EmployeeId); // replace with the actual employee ID

                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();
                    Console.WriteLine($"{rowsAffected} row(s) updated.");
                }

                ViewData["msg"] = "Update success!";

                _context.SaveChanges();

            }
            catch (Exception ex)
            {
                // Log the exception or handle it appropriately
                Console.WriteLine($"Error: {ex.Message}");
            }



            var query = from ep in _context.EmployeeProfiles
                        join a in _context.Accounts on ep.EmployeeId equals a.AccId
                        join r in _context.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName),
                            Employee = a
                        };


            empP = query.Where(x => x.EmployeeId == empP.EmployeeId).FirstOrDefault();
            return Page();
        }
    }
}
