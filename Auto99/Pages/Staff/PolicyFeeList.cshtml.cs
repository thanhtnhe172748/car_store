﻿using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.EntityFrameworkCore;

namespace Auto99.Pages.Staff
{
    public class PolicyFeeListModel : PageModel
    {
        private readonly IFeeRepository _feeRepository;
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;

        public PolicyFeeListModel(Auto99.Models.database_SWP391_Auto99_version35Context context, IFeeRepository feeRepository)
        {
            _context = context;
            _feeRepository = feeRepository;
        }

        public List<PolicyFee> policyFees { get; set; } = default!;
        [BindProperty]
        public string Search { get; set; }

        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }

        //SupportsGet giúp giữ lại data đc giao qua url vd: Staff/BlogList?pageNumber=2&pageSize=25
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = 10; // Adjust the page size as needed

        [BindProperty(SupportsGet = true)]
        public int PageNumber { get; set; } = 1;
        public async Task<IActionResult> OnGetAsync()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            await LoadDataAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormCollection f)
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            int employeeId = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
            string action = f["action"];
            string id = f["feeID"];
            string feeName = f["feeName"];
            string feeNumber = f["fee"];
            int createBy = employeeId;
            if (!string.IsNullOrEmpty(action) && action.Equals("Delete"))
            {
                var Fee = _feeRepository.GetFeebyId(id);
                if (Fee != null)
                {
                    _feeRepository.deleteFee(Fee);
                }
            }
            if (!string.IsNullOrEmpty(action) && action.Equals("UpdateFee"))
            {
                var Fee = _feeRepository.GetFeebyId(id);
                if(Fee != null)
                {
                    _feeRepository.UpdateFee(Fee, feeName, feeNumber, createBy);

                }
            }
            if (!string.IsNullOrEmpty(action) && action.Equals("AddFee"))
            {
                var Fee = _feeRepository.AddFee(feeName, feeNumber, createBy);

            }
            await LoadDataAsync();
            return Page();
        }
        private async Task LoadDataAsync()
        {
            var totalClients = _feeRepository.GetCount();
            TotalPages = (int)Math.Ceiling(totalClients / (double)PageSize);

            policyFees = _feeRepository.GetPolicyFees(PageNumber, PageSize);
            if (!string.IsNullOrEmpty(Search))
            {
                policyFees = policyFees.Where(x => x.FeeName.Contains(Search)).ToList();
            }
        }
    }
}
