using Auto99.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Drawing.Printing;

namespace Auto99.Pages.Staff
{
    public class DesignListModel : PageModel
    {
        database_SWP391_Auto99_version35Context _context= new database_SWP391_Auto99_version35Context();
        public DesignListModel(database_SWP391_Auto99_version35Context context)
        {
            _context = context;
        }
        public List<CarDesign> ListDes { get; set; }
        public void OnGet()
        {
            string pageIndex = "1";
            int index = int.Parse(pageIndex);

            string name = "";

            string quantityShow = "5";
            int quantity = int.Parse(quantityShow);

             ListDes = _context.CarDesigns.Skip((index - 1) * quantity).Take(quantity).ToList();
            int count = _context.CarDesigns.Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }

            ViewData["endPage"]=endPage;
            ViewData["index"]= index;
            ViewData["count"]= count;
            ViewData["quantity"]= quantity;
            ViewData["name"]= name;
        }

        public void OnPost(IFormCollection result)
        {
            string designName = result["designName"];
            string designID = result["designID"];
            string action = result["action"];
            if (!string.IsNullOrEmpty(designName))
            {
                if (action.Equals("add"))
                {
                    CarDesign design = new CarDesign();
                    design.Design=designName;
                    design.CreatedBy = 1;
                    design.CreatedOn=DateTime.Now;
                    _context.CarDesigns.Add(design);
                    _context.SaveChanges();
                }
                else

                if (action.Equals("update"))
                {
                    CarDesign design = _context.CarDesigns.Where(x=>x.DesignId == int.Parse(designID)).FirstOrDefault();
                    design.Design = designName;
                    design.ModifiedBy = 1;
                    design.ModifiedOn = DateTime.Now;
                    _context.SaveChanges();
                }
            }

            string pageIndex = result["index"];
            if (string.IsNullOrEmpty(pageIndex))
            {
                pageIndex = "1";
            }
            int index = int.Parse(pageIndex);

            string name = result["name"];
            if (string.IsNullOrEmpty(name))
            {
                name = "";
            }

            string quantityShow = result["quantity"];
            if (string.IsNullOrEmpty(quantityShow))
            {
                quantityShow = "5";
            }
            int quantity = int.Parse(quantityShow);

            ListDes = _context.CarDesigns.Where(x=>x.Design.Contains(name)).Skip((index - 1) * quantity).Take(quantity).ToList();
            int count = _context.CarDesigns.Where(x => x.Design.Contains(name)).Count();
            int endPage = count / quantity;
            if (count % quantity != 0)
            {
                endPage++;
            }


            ViewData["endPage"] = endPage;
            ViewData["index"] = index;
            ViewData["count"] = count;
            ViewData["quantity"] = quantity;
            ViewData["name"] = name;
        }
    }
}
