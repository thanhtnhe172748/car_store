using Auto99.Models;
using Auto99.Services;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.VisualStudio.Debugger.Contracts;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class CarEditModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;
        private readonly ICarRepository _carRepository;

        public CarEditModel(Auto99.Models.database_SWP391_Auto99_version35Context context, ICarRepository carRepository)
        {
            _context = context;
            _carRepository = carRepository;
        }
        public List<CarDesign> DesignList { get; set; } 
        public List<CarBrand> BrandList { get; set; }
        public List<StatusCategory> StatusList { get; set; }
        public List<CarStaffPage> CarList { get; set; }
        public CarStaffPage car { get; set; }
        public GeneralInfoCar gic { get; set; }
        public EngineAndChassi ei { get; set; }
        public CarImg ci { get;set; }
        public CarSubImg csi { get;set; }
        public IActionResult OnGetAsync(string action, int carID)
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            string Role = HttpContext.Session.GetString("Role");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            if (string.IsNullOrEmpty(Role) || !Role.Equals("admin"))
            {
                return RedirectToPage("/Staff/AdminHome");
            }
            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();
            car = _carRepository.GetCarByID(carID);
            gic = _carRepository.GetGeneralInfoCars().Where(x=>x.CarId == carID).FirstOrDefault();
            ei = _carRepository.GetEngineAndChassis().Where(x => x.CarId == carID).FirstOrDefault();
            ci = _carRepository.GetAllCarImgs().Where(x => x.CarId == carID).FirstOrDefault();
            csi = _carRepository.GetAllCarSubImgs().Where(x => x.CarId == carID).FirstOrDefault();
            return Page();
        }

        public IActionResult OnPost(IFormCollection result)
        {
            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();
            ViewData["mess"] = "";
            ViewData["messSubIMG"] = "";
            ViewData["textareaData"] = "";
            ViewData["msg"] = "";
            string carId = result["carId"];
            string carName = result["carName"];
            string statusID = result["statusID"];
            string designID = result["designID"];
            string brandID = result["brandID"];
            string madeIn = result["madeIn"];
            string capacity = result["capacity"];
            string engineType = result["engineType"];
            string cylinderNumber = result["cylinderNumber"];
            string cylinderMaxCapacity = result["cylinderMaxCapacity"];
            string variableVale = result["variableVale"];
            string fuel = result["fuel"];
            string fuelSystem = result["fuelSystem"];
            string fuelCapacity = result["fuelCapacity"];
            string maxTorque = result["maxTorque"];
            string gear = result["gear"];
            string price = result["price"];
            string seatNumber = result["seatNumber"];
            string description = result["description"];

            //get parameter main image of the product
            IFormFile partFileImageUpload = result.Files["ImageUpload"];
            IFormFile partFileSubImageUpload = result.Files["SubImageUpload"];
            string ImageUpload = "";
            string SubImageUpload = "";

            string allowedExtensions = ".jpg|.jpeg|.png|.gif";

            if (partFileImageUpload == null)
            {
                ImageUpload = "";
            }
            else
            {
                ImageUpload = partFileImageUpload.FileName;
            }

            if (partFileSubImageUpload == null)
            {
                SubImageUpload = "";
            }
            else
            {
                SubImageUpload = partFileSubImageUpload.FileName;
            }

            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            int employeeId = _context.EmployeeProfiles.Where(x => x.Email.Equals(StaffEmail)).Select(x => x.EmployeeId).FirstOrDefault();
            _carRepository.UpdateCar(carName, price, brandID, statusID, int.Parse(carId), employeeId);
            _carRepository.UpdateEngine(carId, capacity, engineType, cylinderNumber, cylinderMaxCapacity, variableVale, fuelSystem, fuelCapacity, maxTorque, gear);
            _carRepository.UpdateGeneralInfoCar(carId, seatNumber, designID, madeIn, fuel, description);

            FileProcess lo = new FileProcess();
            ViewData["notification"] = "Update thành công";
            if (!string.IsNullOrEmpty(ImageUpload))
            {
                    string fileExtension = Path.GetExtension(partFileImageUpload.FileName).Trim();
                if (allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    // Generate a unique folder name based on a timestamp or some other identifier.
                    string newCarIMGFolder = carId;

                    // Define the directory where the image will be saved.
                    StringBuilder absolutePathDirectory = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGCAR);

                    // Create the directory if it doesn't exist.
                    DirectoryInfo directory = new DirectoryInfo(absolutePathDirectory + newCarIMGFolder);
                    if (!directory.Exists)
                    {
                        directory.Create();
                    }

                    // Build the absolute path to the image file.
                    string absolutePath = absolutePathDirectory + newCarIMGFolder + "\\";

                    //load image to file img


                    //copy ảnh mới vào folder
                    string newNameImage = "ImageUpload_" + carId + fileExtension;
                    string destFile = Path.Combine(absolutePath, newNameImage);
                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        partFileImageUpload.CopyTo(stream);
                    }


                    _carRepository.UpdateImage(carId, newNameImage);
                }
                else
                {
                    ViewData["mess"] = "File không hợp lệ";
                }
            }
            if (!string.IsNullOrEmpty(SubImageUpload))
            {
                string fileExtension = Path.GetExtension(partFileSubImageUpload.FileName).Trim();
                if (allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    // Generate a unique folder name based on a timestamp or some other identifier.
                    string newCarIMGFolder = carId;

                    // Define the directory where the image will be saved.
                    StringBuilder absolutePathDirectory = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGCAR);

                    // Create the directory if it doesn't exist.
                    DirectoryInfo directory = new DirectoryInfo(absolutePathDirectory + newCarIMGFolder);
                    if (!directory.Exists)
                    {
                        directory.Create();
                    }

                    // Build the absolute path to the image file.
                    string absolutePath = absolutePathDirectory + newCarIMGFolder + "\\";

                    //load image to file img


                    //copy ảnh mới vào folder
                    string newNameImage = "ImageSubUpload_" + carId + fileExtension;
                    string destFile = Path.Combine(absolutePath, newNameImage);
                    using (var stream = new FileStream(destFile, FileMode.Create))
                    {
                        partFileSubImageUpload.CopyTo(stream);
                    }


                    _carRepository.UpdateSubImage(carId, newNameImage);
                }
                else
                {
                    ViewData["mess"] = "File không hợp lệ";
                }
            }
        

            car = _carRepository.GetCarByID(int.Parse(carId));
            gic = _carRepository.GetGeneralInfoCars().Where(x => x.CarId == int.Parse(carId)).FirstOrDefault();
            ei = _carRepository.GetEngineAndChassis().Where(x => x.CarId == int.Parse(carId)).FirstOrDefault();
            ci = _carRepository.GetAllCarImgs().Where(x => x.CarId == int.Parse(carId)).FirstOrDefault();
            csi = _carRepository.GetAllCarSubImgs().Where(x => x.CarId == int.Parse(carId)).FirstOrDefault();
            return Page();
        }
    }
}
