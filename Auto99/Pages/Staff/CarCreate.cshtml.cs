using Auto99.Models;
using Auto99.Services;
using Auto99.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.IO;
using System.Linq;
using System.Text;

namespace Auto99.Pages.Staff
{
    public class CarCreateModel : PageModel
    {
        private readonly Auto99.Models.database_SWP391_Auto99_version35Context _context;
        private readonly ICarRepository _carRepository;

        public CarCreateModel(Auto99.Models.database_SWP391_Auto99_version35Context context, ICarRepository carRepository)
        {
            _context = context;
            _carRepository = carRepository;
        }
        public List<CarDesign> DesignList { get; set; }
        public List<CarBrand> BrandList { get; set; }
        public List<StatusCategory> StatusList { get; set; }
        public List<CarStaffPage> CarList { get; set; }
        public IActionResult OnGet()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            string Role = HttpContext.Session.GetString("Role");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            if (string.IsNullOrEmpty(Role) || !Role.Equals("admin"))
            {
                return RedirectToPage("/Staff/AdminHome");
            }
            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();

            ViewData["mess"] = "";
            ViewData["messSubIMG"] = "";
            ViewData["textareaData"] = "";
            ViewData["msg"] = "";
            return Page();
        }

        public IActionResult OnPost(IFormCollection result)
        {
            DesignList = _carRepository.GetCarDesigns();
            BrandList = _carRepository.GetAllCarBrand();
            StatusList = _carRepository.GetAllStatus();
            ViewData["mess"] = "";
            ViewData["messSubIMG"] = "";
            ViewData["textareaData"] = "";
            ViewData["msg"] = "";
            string carName = result["carName"];
            string statusID = result["statusID"];
            string designID = result["designID"];
            string brandID = result["brandID"];
            string madeIn = result["madeIn"];
            string capacity = result["capacity"];
            string engineType = result["engineType"];
            string cylinderNumber = result["cylinderNumber"];
            string cylinderMaxCapacity = result["cylinderMaxCapacity"];
            string variableVale = result["variableVale"];
            string fuel = result["fuel"];
            string fuelSystem = result["fuelSystem"];
            string fuelCapacity = result["fuelCapacity"];
            string maxTorque = result["maxTorque"];
            string gear = result["gear"];
            string price = result["price"];
            string seatNumber = result["seatNumber"];
            string description = result["description"];

            //get parameter main image of the product
            IFormFile partFileImageUpload = result.Files["ImageUpload"];
            IFormFile partFileSubImageUpload = result.Files["SubImageUpload"];
            string ImageUpload = "";
            string SubImageUpload = "";

            string allowedExtensions = ".jpg|.jpeg|.png|.gif";

            if (partFileImageUpload == null)
            {
                ImageUpload = "";
            }
            else
            {
                ImageUpload = partFileImageUpload.FileName;
            }

            if (partFileSubImageUpload == null)
            {
                SubImageUpload = "";
            }
            else
            {
                SubImageUpload = partFileSubImageUpload.FileName;
            }

            if (!string.IsNullOrEmpty(ImageUpload))
            {
                if (!string.IsNullOrEmpty(SubImageUpload))
                {
                    string fileExtension = Path.GetExtension(partFileImageUpload.FileName).Trim();
                    string subfileExtension = Path.GetExtension(partFileSubImageUpload.FileName).Trim();
                    if (allowedExtensions.Contains(fileExtension.ToLower()) && allowedExtensions.Contains(subfileExtension.ToLower()))
                    {
                        string StaffEmail = HttpContext.Session.GetString("StaffEmail");
                        int employeeId = _context.EmployeeProfiles.Where(x=>x.Email.Equals(StaffEmail)).Select(x=>x.EmployeeId).FirstOrDefault();
                        //add new car
                        int carID = _carRepository.AddCar(carName, price, brandID, statusID, employeeId);
                        string CarID = carID.ToString();
                        _carRepository.AddEngine(carID, capacity, engineType, cylinderNumber, cylinderMaxCapacity, variableVale, fuelSystem, fuelCapacity, maxTorque, gear);
                        _carRepository.AddGeneralInfoCar(carID, seatNumber, designID, madeIn, fuel, description);
                        // Generate a unique folder name based on a timestamp or some other identifier.
                        string newCarIMGFolder = CarID;

                        // Define the directory where the image will be saved.
                        FileProcess lo = new FileProcess();
                        StringBuilder absolutePathDirectory = lo.GetRelativePath(lo.LOADIMAGETOFILEIMGCAR);

                        // Create the directory if it doesn't exist.
                        DirectoryInfo directory = new DirectoryInfo(absolutePathDirectory + newCarIMGFolder);
                        if (!directory.Exists)
                        {
                            directory.Create();
                        }

                        // Build the absolute path to the image file.
                        string absolutePath = absolutePathDirectory + newCarIMGFolder + "\\";

                        //load image to file img


                        //copy ảnh mới vào folder
                        string newNameImage = "ImageUpload_" + CarID + fileExtension;
                        string newSubNameImage = "ImageSubUpload_" + CarID + subfileExtension;
                        string destFile = Path.Combine(absolutePath, newNameImage);
                        string destSubFile = Path.Combine(absolutePath, newSubNameImage);
                        using (var stream = new FileStream(destFile, FileMode.Create))
                        {
                            partFileImageUpload.CopyTo(stream);
                        }
                        
                        using (var stream = new FileStream(destSubFile, FileMode.Create))
                        {
                            partFileSubImageUpload.CopyTo(stream);
                        }


                        _carRepository.AddImage(carID, newNameImage);
                        _carRepository.AddSubImage(carID, newSubNameImage);
                        return Redirect("/Staff/CarList");

                    }
                    else
                    {
                        ViewData["mess"]="File không hợp lệ";
                    }
                }
                else
                {
                    ViewData["messSubIMG"] = "Không tìm thấy file";
                }
            }
            else
            {
                ViewData["mess"] = "Không tìm thấy file";
            }
            return Page();
        }
    }
}
