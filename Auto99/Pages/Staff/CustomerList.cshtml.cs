﻿using Auto99.Models;
using Auto99.Repositories;
using Auto99.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Auto99.Pages.Staff
{
    public class CustomerListModel : PageModel
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerListModel(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        [BindProperty]
        public string Search { get; set; }
        public IList<Client> clientList { get; set; } = default!;


        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }

        //SupportsGet giúp giữ lại data đc giao qua url vd: Staff/BlogList?pageNumber=2&pageSize=25
        [BindProperty(SupportsGet = true)]
        public int PageSize { get; set; } = 10; // Adjust the page size as needed

        [BindProperty(SupportsGet = true)]
        public int PageNumber { get; set; } = 1;


        [BindProperty]
        public string Action { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            string StaffEmail = HttpContext.Session.GetString("StaffEmail");
            if (string.IsNullOrEmpty(StaffEmail))
            {
                return RedirectToPage("/Staff/Login");
            }
            //Status = string.Empty;
            //Parent = string.Empty;
            await LoadDataAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(IFormCollection f)
        {
            //if (Action != null && Action == "UpdateBlog")
            //{
            //    int id = int.Parse(f["blogID"]);

            //    var findBlog = _context.Blogs.Find(id);
            //    if (findBlog != null)
            //    {
            //        findBlog.Status = bool.Parse(Status);
            //        _context.SaveChanges();
            //    }
            //}
            await LoadDataAsync();
            return Page();
        }

        private async Task LoadDataAsync()
        {
            var totalClients = _customerRepository.GetCount();
            TotalPages = (int)Math.Ceiling(totalClients / (double)PageSize);
            
            clientList = _customerRepository.GetClients(PageNumber,PageSize);
            if (!string.IsNullOrEmpty(Search))
            {
                clientList = clientList.Where(x => x.ClientName.Contains(Search)).ToList();
            }
        }
    }
}
