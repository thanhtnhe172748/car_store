﻿document.addEventListener('DOMContentLoaded', function () {
    const personalRadio = document.getElementById('personalRadio');
    const corporateRadio = document.getElementById('corporateRadio');
    const groupPersonal = document.querySelectorAll('.group-personal');
    const groupCorporate = document.querySelectorAll('.group-corporate');

    // Set default visibility on page load
    if (personalRadio.checked) {
        groupPersonal.forEach(element => element.style.display = 'block');
        groupCorporate.forEach(element => element.style.display = 'none');
    } else if (corporateRadio.checked) {
        groupPersonal.forEach(element => element.style.display = 'none');
        groupCorporate.forEach(element => element.style.display = 'block');
    }

    personalRadio.addEventListener('change', function () {
        groupPersonal.forEach(element => element.style.display = 'block');
        groupCorporate.forEach(element => element.style.display = 'none');
    });

    corporateRadio.addEventListener('change', function () {
        groupPersonal.forEach(element => element.style.display = 'none');
        groupCorporate.forEach(element => element.style.display = 'block');
    });
});

function checkVoucher() {
    const voucher = document.getElementById('voucher').value;
    // Call your API to validate the voucher and update final price
    console.log('Checking voucher:', voucher);
    // Update the final price based on voucher validation
    const finalPriceElement = document.getElementById('finalPriceValue');
    const price = parseInt(document.getElementById('price').value);
    // Example logic for voucher
    if (voucher === 'DISCOUNT50') {
        finalPriceElement.value = price / 2;
    } else {
        finalPriceElement.value = price;
    }
    document.getElementById('finalPrice').querySelector('p span').innerText = finalPriceElement.value.toLocaleString() + ' VNĐ';
}
