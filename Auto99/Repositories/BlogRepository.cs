﻿using Auto99.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Auto99.Services
{
    public interface IBlogRepository
    {
        List<Blog> GetAllBlog();
        List<BlogCategory> GetAllBlogCategory();
        List<List<Blog>> AllBlogsByPI(List<Blog> ListBlog);
        List<Blog> GetBlogByParentID(int? parentID);
        List<Blog> GetBlogByCategoryID(int categoryID);
        public List<Blog> GetFooter();
        Dictionary<int, int> GetParentIDCountMap(List<Blog> blogs);
        List<Blog> GetListBlogID4(int id);
        int GetLastID();
        void AddBlog(string title, int parentID, string description, string content,string imgname,int createby);
        object GetBlogCategoryByThanh();
        Blog getBlogbyBlogId(int id);
        bool UpdateBlogById(int blogId, string title, int parentID, string description, string content, string imgName, int modifiedBy);
        List<Blog> GetAllTTaKm();
        List<Blog> GetAllSameParentIDBlogByBlogID(int blogID);
        List<Blog> GetListBlogTTaKM(int index, List<string> listParentID);
        int GetTotalAllSameParentIDBlogByBlogID(int blogID);
        int GetTotalBlogTTaKM(List<string> listParentID);
        List<Blog> GetListAds();
        Blog GetAd(int parentID);
        List<Blog> GetAllTrueBlogs();
    }
        
    public class BlogRepository : IBlogRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public BlogRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }

        public bool UpdateBlogById(int blogId, string title, int parentID, string description, string content, string imgName, int modifiedBy)
        {
            var blog = _dbContext.Blogs.Find(blogId);
            if(blog != null) {
                blog.Title = title;
                blog.ParentId = parentID;
                blog.Description = description;
                blog.Content = content;
                blog.ModifiedBy = modifiedBy;
                blog.BlogImg = imgName;

                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public Blog getBlogbyBlogId(int id)
        {
            Blog blog = _dbContext.Blogs.Include(x=>x.BlogCategory).Where(x=>x.BlogId == id).FirstOrDefault();
            return blog;
        }
        public void AddBlog(string title, int parentID, string description, string content,string img, int createby)
        {
            var newBlog = new Blog
            {
                Title = title,
                ParentId = parentID,
                Description = description,
                Content = content,
                CreatedOn = DateTime.Now, // Hoặc giá trị thời gian khác mà bạn muốn
                BlogImg = img,
                CreatedBy = createby,
                Status = true
                // Các thuộc tính khác nếu có
            };

            _dbContext.Blogs.Add(newBlog);
            _dbContext.SaveChanges();
        }
        public List<Blog> GetAllBlog()
        {
            List<Blog> allBlog = _dbContext.Blogs.ToList();
            return allBlog;
        }
        public int GetLastID()
        {
            int id = _dbContext.Blogs.OrderByDescending(x=>x.BlogId).Select(x=>x.BlogId).FirstOrDefault();
            return id;
        }
        public List<BlogCategory> GetAllBlogCategory()
        {
            return _dbContext.BlogCategories.ToList();
        }
        public List<List<Blog>> AllBlogsByPI(List<Blog> ListBlog)
        {
            var allBlogByPI = new List<List<Blog>>();
            foreach (var category in ListBlog)
            {
                if (category.BlogCategoryId == 4)
                {
                    var blogs = _dbContext.Blogs.Where(b => b.ParentId == category.ParentId && b.BlogCategoryId != category.BlogCategoryId).ToList();
                    allBlogByPI.Add(blogs);
                }
                else
                {
                    allBlogByPI.Add(new List<Blog>());
                }
            }
            return allBlogByPI;
        }
        public List<Blog> GetBlogByParentID(int? parentID)
        {
            var blogs = _dbContext.Blogs
                .Include(b => b.BlogCategory)
                .Where(b => b.ParentId == parentID && b.Status== true)
                .OrderByDescending(b => b.CreatedOn)
                .ToList();

            return blogs;
        }
        public List<Blog> GetBlogByCategoryID(int categoryID)
        {
            var blogs = _dbContext.Blogs
                .Include(b => b.BlogCategory)
                .Where(b => b.BlogCategoryId == categoryID)
                .ToList();

            return blogs;
        }
        public List<Blog> GetFooter()
        {
            return _dbContext.Blogs
                .Where(b => b.ParentId == 15 && b.BlogCategoryId == null)
                .ToList();
        }
        public Dictionary<int, int> GetParentIDCountMap(List<Blog> blogs)
        {
            return blogs.GroupBy(b => b.ParentId)
                        .ToDictionary(g => g.Key, g => g.Count());
        }

        public object GetBlogCategoryByThanh()
        {
            var categoryQuery = from b in _dbContext.Blogs
                                where (from blog in _dbContext.Blogs
                                       group blog by blog.ParentId into g
                                       select g.Key).Contains(b.ParentId)
                                      && b.BlogCategoryId != null
                                select new { b.Title, b.ParentId };

             var catelist = categoryQuery.ToList();
            return catelist;
        }
        public List<Blog> GetAllTTaKm()
        {
            try
            {
                var query = from b in _dbContext.Blogs
                            join bc in _dbContext.BlogCategories on b.BlogCategoryId equals bc.BlogCategoryId
                            where b.BlogCategoryId == 3
                            orderby b.CreatedOn descending
                            select new Blog
                            {
                                BlogId = b.BlogId,
                                BlogCategoryId = b.BlogCategoryId,
                                BlogCategory = bc, // Assuming BlogCategory is a navigation property
                                Title = b.Title,
                                Description = b.Description,
                                Content = b.Content,
                                CreatedOn = b.CreatedOn,
                                CreatedBy = b.CreatedBy,
                                CreatedByNavigation = b.CreatedByNavigation, // Assuming CreatedByAccount is a navigation property
                                ModifiedOn = b.ModifiedOn,
                                ModifiedBy = b.ModifiedBy,
                                ModifiedByNavigation = b.ModifiedByNavigation, // Assuming ModifiedByAccount is a navigation property
                                Status = b.Status,
                                BlogImg = b.BlogImg,
                                ParentId = b.ParentId,
                                Url = b.Url
                            };

                return query.ToList();
            }
            catch (Exception ex)
            {
                // Handle exceptions as needed
                Console.WriteLine("Error: " + ex.Message);
                return new List<Blog>(); // or return null or throw an exception
            }
        }
        public List<Blog> GetAllSameParentIDBlogByBlogID(int blogID)
        {
                try
                {
                    var query = from b in _dbContext.Blogs
                                where b.ParentId == _dbContext.Blogs.FirstOrDefault(x => x.BlogId == blogID).ParentId
                                      && b.BlogCategoryId == null
                                      && b.Status == true
                                orderby b.CreatedOn descending
                                select new Blog
                                {
                                    BlogId = b.BlogId,
                                    BlogCategoryId = b.BlogCategoryId,
                                    BlogCategory = b.BlogCategory, // Assuming BlogCategory is a navigation property
                                    Title = b.Title,
                                    Description = b.Description,
                                    Content = b.Content,
                                    CreatedOn = b.CreatedOn,
                                    CreatedBy = b.CreatedBy,
                                    CreatedByNavigation = b.CreatedByNavigation, // Assuming CreatedByAccount is a navigation property
                                    ModifiedOn = b.ModifiedOn,
                                    ModifiedBy = b.ModifiedBy,
                                    ModifiedByNavigation = b.ModifiedByNavigation, // Assuming ModifiedByAccount is a navigation property
                                    Status = b.Status,
                                    BlogImg = b.BlogImg,
                                    ParentId = b.ParentId,
                                    Url = b.Url
                                };

                    return query.ToList();
                }
                catch (Exception ex)
                {
                    // Handle exceptions as needed
                    Console.WriteLine("Error: " + ex.Message);
                    return new List<Blog>(); // or return null or throw an exception
                }
        }
        public List<Blog> GetListBlogTTaKM(int index, List<string> listParentID)
        {
                try
                {
                    int pageSize = 6;
                    int skip = (index - 1) * pageSize;

                    var blogs = _dbContext.Blogs
                        .Where(b => listParentID.Contains(b.ParentId.ToString()) && b.BlogCategoryId == null && b.Status == true)
                        .OrderByDescending(b => b.CreatedOn)
                        .Skip(skip)
                        .Take(pageSize)
                        .Include(b => b.BlogCategory)
                        .Include(b => b.CreatedByNavigation)
                        .Include(b => b.ModifiedByNavigation)
                        .ToList();

                    return blogs;
                }
                catch (Exception ex)
                {
                    // Handle exceptions as needed
                    Console.WriteLine("Error: " + ex.Message);
                    return new List<Blog>(); // or return null or throw an exception
                }
        }
        public int GetTotalAllSameParentIDBlogByBlogID(int blogID)
        {
                try
                {
                    var count = _dbContext.Blogs
                        .Where(b => b.ParentId == _dbContext.Blogs.FirstOrDefault(x => x.BlogId == blogID).ParentId && b.BlogCategoryId == null)
                        .Count();

                    return count;
                }
                catch (Exception ex)
                {
                    // Handle exceptions as needed
                    Console.WriteLine("Error: " + ex.Message);
                    return 0; // or throw an exception
                }
        }
        public int GetTotalBlogTTaKM(List<string> listParentID)
        {
                try
                {
                    var count = _dbContext.Blogs
                        .Where(b => listParentID.Contains(b.ParentId.ToString()) && b.BlogCategoryId == null)
                        .Count();

                    return count;
                }
                catch (Exception ex)
                {
                    // Handle exceptions as needed
                    Console.WriteLine("Error: " + ex.Message);
                    return 0; // or throw an exception
                }
        }
        public List<Blog> GetListAds()
        {
            List<Blog> list = new List<Blog>();

            try
            {
                // Assuming your DbContext is named ApplicationDbContext and you have a DbSet<Blog> named Blogs
                var query = _dbContext.Blogs
                    .Where(b => b.BlogCategoryId == null && b.CreatedBy != null)
                    .OrderByDescending(b => b.CreatedOn)
                    .Take(10)
                    .ToList();

                // Map the results to your Blog model
                foreach (var blog in query)
                {
                    Blog a = new Blog
                    {
                        BlogId = blog.BlogId,
                        BlogCategoryId = blog.BlogCategoryId,
                        Title = blog.Title,
                        Description = blog.Description,
                        CreatedOn = blog.CreatedOn,
                        BlogImg = blog.BlogImg, // Assuming BlogIMG is a property in your Blog model
                        ParentId = blog.ParentId
                    };
                    list.Add(a);
                }

                return list;
            }
            catch (Exception e)
            {
                // Handle exceptions as per your application's error handling strategy
                Console.WriteLine(e.Message); // Example: Log the exception message
                return null; // Or handle error appropriately
            }
        }
        public Blog GetAd(int parentID)
        {
            try
            {
                // Assuming your DbContext is named ApplicationDbContext and you have a DbSet<Blog> named Blogs
                var blog = _dbContext.Blogs
                    .Where(b => b.ParentId == parentID && b.Status == true && b.BlogCategoryId == null)
                    .OrderByDescending(b => b.CreatedOn)
                    .FirstOrDefault();

                if (blog != null)
                {
                    return new Blog
                    {
                        BlogId = blog.BlogId,
                        BlogCategoryId = blog.BlogCategoryId,
                        Title = blog.Title,
                        CreatedOn = blog.CreatedOn,
                        ParentId = blog.ParentId,
                        BlogImg = blog.BlogImg // Assuming BlogIMG is a property in your Blog model
                    };
                }
            }
            catch (Exception e)
            {
                // Handle exceptions as per your application's error handling strategy
                Console.WriteLine(e.Message); // Example: Log the exception message
            }

            return null;
        }
        public List<Blog> GetAllTrueBlogs()
        {
            return _dbContext.Blogs
                .Where(b => b.Status == true)
                .ToList();
        }
        public List<Blog> GetListBlogID4(int id)
        {
            return _dbContext.Blogs.Where(x => x.BlogCategoryId == id).ToList();
        }
    }
}
