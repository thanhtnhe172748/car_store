﻿using Auto99.Models;
using System.Drawing.Printing;

namespace Auto99.Repositories
{
    public interface ICustomerRepository
    {
        List<Client> GetClients(int PageNumber, int PageSize);
        int GetCount();

    }
    public class CustomerRepository : ICustomerRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public CustomerRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Client> GetClients(int PageNumber, int PageSize)
        {
            var list = _dbContext.Clients.Skip((PageNumber - 1) * PageSize)
                .Take(PageSize)
                .ToList(); 
            return list;
        }

        public int GetCount()
        {
            var count = _dbContext.Clients.Count();
            return count;
        }
    }
}
