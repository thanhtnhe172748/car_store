﻿using Auto99.Message;
using Auto99.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using NuGet.Packaging.Signing;
using System.Configuration;
using System.Drawing.Printing;
using System.Text.RegularExpressions;

namespace Auto99.Repositories
{
    public interface IFeeRepository
    {
        bool AddFee(string feeName, string feeNumber, int createBy);
        void deleteFee(PolicyFee fee);
        double GetCount();
        PolicyFee GetFeebyId(string id);
        List<PolicyFee> GetPolicyFees(int pageNumber, int pageSize);
        void UpdateFee(PolicyFee fee, string feeName, string feeNumber, int modifyby);
        public List<PolicyFee> GetListFee();
    }
    public class FeeRepository : IFeeRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public FeeRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }

        public bool AddFee(string feeName, string feeNumber, int createBy)
        {
            var newFee = new PolicyFee()
            {
                FeeName = feeName,
                Fee = double.Parse(feeNumber),
                CreatedBy = createBy,
                CreatedOn = DateTime.Now
            };
            _dbContext.PolicyFees.Add(newFee);
            _dbContext.SaveChanges();
            return true;
        }

        public void deleteFee(PolicyFee fee)
        {
            _dbContext.PolicyFees.Remove(fee);
            _dbContext.SaveChanges();
        }

        public double GetCount()
        {
            var count = _dbContext.PolicyFees.Count();
            return count;
        }

        public PolicyFee GetFeebyId(string id)
        {
            var fee = _dbContext.PolicyFees.Find(int.Parse(id));
            return fee;
        }

        public List<PolicyFee> GetPolicyFees(int PageNumber, int PageSize)
        {
            var list = _dbContext.PolicyFees.Skip((PageNumber - 1) * PageSize)
                .Take(PageSize).Include(x=>x.ModifiedByNavigation)
                .ToList();
            return list;
        }

        public void UpdateFee(PolicyFee fee, string feeName, string feeNumber, int modifyby)
        {
            fee.FeeName = feeName;
            fee.Fee = double.Parse(feeNumber);
            fee.ModifiedBy = modifyby;
            fee.ModifiedOn = DateTime.Now;
            _dbContext.SaveChanges();
        }
        public List<PolicyFee> GetListFee()
        {
            return _dbContext.PolicyFees.ToList();
        }
    }
}

