﻿using Auto99.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Debugger.Contracts;
using System;
using System.Xml.Linq;

namespace Auto99.Services
{
    public interface ICarRepository
    {
        List<CarDesign> GetCarDesigns();
        List<Car> GetCarsByDesignID(int designID);
        List<CarImg> GetAllCarImgs();
        List<CarSubImg> GetAllCarSubImgs();
        List<EngineAndChassi> GetEngineAndChassis();
        List<GeneralInfoCar> GetGeneralInfoCars();
        //---------------LinhTB2 add func cho CarList-------------------
        double GetMaxPriceCar(string name, int brandID, int designID, int statusID);
        List<CarBrand> GetAllCarBrand();
        List<StatusCategory> GetAllStatus();
        List<CarStaffPage> GetListCarAdmin(int index, string name, int quantityShow, int brandID, int designID, int statusID, double minPrice, double maxPrice);
        int GetTotalCarAdmin(string name, int brandID, int designID, int statusID, double minPrice, double maxPrice);
        void UpdateCar(string carName, string price, string statusID, int carID, int modifiedBy);
        void UpdateCar(string carName, string price, string brandID, string statusID, int carID, int modifiedBy);
        int AddCar(string carName, string price, string brandID, string statusID, int CreateBy);
        void AddEngine(int carID, string fuelTankCapacity, string engineType, string numberOfCylinder, string cylinderCapacity, string variableValveSystem, string fuelSystem, string maximumCapacity, string maximumTorque, string gear);
        void AddGeneralInfoCar(int carID, string seatNumber, string designID, string madeIn, string fuel, string description);
        void AddImage(int carID, string imageUpload);
        void AddSubImage(int carID, string subImageUpload);
        CarStaffPage GetCarByID(int carid);
        void UpdateEngine(string carId, string capacity, string engineType, string cylinderNumber, string cylinderMaxCapacity, string variableVale, string fuelSystem, string fuelCapacity, string maxTorque, string gear);
        void UpdateGeneralInfoCar(string carId, string seatNumber, string designID, string madeIn, string fuel, string description);
        void UpdateImage(string carId, string newNameImage);
        void UpdateSubImage(string carId, string newSubNameImage);
        List<CarBrand> GetCarBrands();
        List<Car> GetAllCars();
        Car GetCarById(int id);
        EngineAndChassi GetEngineByCarId(int id);
        GeneralInfoCar GetGeneralInfoCarByCarId(int id);
        List<Car> GetListCarCE(int brandID, int designID);
        double GetCarPrice(int carID);
        CarSubImg GetCarSubImg(int carID);
        void AddOrder(String carorderCode, int clientID, int carID, bool paymentType, long orderValue, long paid, long paymentRequired, DateTime createdOn, DateTime orderDate, String status);
        CarBrand GetBrandById(int Id);
        CarDesign GetDesignById(int Id);
    }
    public class CarRepository : ICarRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;
        public static string getConnectionString()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).

                AddJsonFile("appsettings.json", optional: false);

            IConfiguration con = builder.Build();

            return con.GetValue<string>("ConnectionStrings:DbString");

        }
        public CarRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }
        public List<CarDesign> GetCarDesigns()
        {
            return _dbContext.CarDesigns.ToList();
        }
        public List<Car> GetCarsByDesignID(int designID)
        {
            var query = from c in _dbContext.Cars
                        join gi in _dbContext.GeneralInfoCars on c.CarId equals gi.CarId
                        join cd in _dbContext.CarDesigns on gi.DesignId equals cd.DesignId
                        join eac in _dbContext.EngineAndChasses on c.CarId equals eac.CarId
                        where gi.DesignId == designID
                        where c.StatusId == 1
                        orderby c.CarId descending
                        select new Car
                        {
                            CarId = c.CarId,
                            CarName = c.CarName,
                            Price = c.Price,
                        };

            return query.Take(10).Distinct().ToList();
        }
        public List<CarImg> GetAllCarImgs()
        {
            return _dbContext.CarImgs.ToList();
        }
        public List<CarSubImg> GetAllCarSubImgs()
        {
            return _dbContext.CarSubImgs.ToList();
        }
        public List<EngineAndChassi> GetEngineAndChassis()
        {
            return _dbContext.EngineAndChasses.ToList();
        }
        public List<GeneralInfoCar> GetGeneralInfoCars()
        {
            return _dbContext.GeneralInfoCars.ToList();
        }

        //---------------LinhTB2 add func cho CarList-------------------
        public double GetMaxPriceCar(string name, int brandID, int designID, int statusID)
        {
            var query = from c in _dbContext.Cars
                        join cb in _dbContext.CarBrands on c.BrandId equals cb.BrandId into cbList
                        from cb in cbList.DefaultIfEmpty()
                        join cs in _dbContext.StatusCategories on c.StatusId equals cs.StatusId into csList
                        from cs in csList.DefaultIfEmpty()
                        join gi in _dbContext.GeneralInfoCars on c.CarId equals gi.CarId into giList
                        from gi in giList.DefaultIfEmpty()
                        join cd in _dbContext.CarDesigns on gi.DesignId equals cd.DesignId into cdList
                        from cd in cdList.DefaultIfEmpty()
                        select new
                        {
                            Car = c,
                            CarBrand = cb,
                            StatusCategory = cs,
                            GeneralInfoCar = gi,
                            CarDesign = cd
                        };

            //string[] keywords = name.Split("\\s+"); // Tách các từ khóa trong title
            //if (!string.IsNullOrEmpty(name))
            //{
            //    for (int i = 0; i < keywords.Length; i++)
            //    {
            //        query = query.Where(x => x.Car.CarName.Contains(keywords[i])
            //                               || x.CarBrand.BrandName.Contains(keywords[i])
            //                               || x.CarDesign.Design.Contains(keywords[i])
            //                            );
            //    }
            //}
            
            //if (brandID != 0)
            //{
            //    query = query.Where(x => x.Car.BrandId == brandID);
            //}

            //if (designID != 0)
            //{
            //    query = query.Where(x => x.CarDesign.DesignId == designID);
            //}

            //if (statusID != 0)
            //{
            //    query = query.Where(x => x.StatusCategory.StatusId == statusID);
            //}

            double maxPrice = 0;
            foreach(var c in query)
            {
                if (c.Car.Price!=null && c.Car.Price > maxPrice) maxPrice = c.Car.Price;
            }

            return maxPrice;
        }
        public List<CarBrand> GetAllCarBrand()
        {
            return _dbContext.CarBrands.ToList();
        }
        public List<StatusCategory> GetAllStatus()
        {
            return _dbContext.StatusCategories.ToList();
        }
        public List<CarStaffPage> GetListCarAdmin(int index, string name, int quantityShow, int brandID, int designID, int statusID, double minPrice, double maxPrice)
        {
            var query = from c in _dbContext.Cars
                        join cb in _dbContext.CarBrands on c.BrandId equals cb.BrandId into cbList
                        from cb in cbList.DefaultIfEmpty()
                        join cs in _dbContext.StatusCategories on c.StatusId equals cs.StatusId into csList
                        from cs in csList.DefaultIfEmpty()
                        join gi in _dbContext.GeneralInfoCars on c.CarId equals gi.CarId into giList
                        from gi in giList.DefaultIfEmpty()
                        join cd in _dbContext.CarDesigns on gi.DesignId equals cd.DesignId into cdList
                        from cd in cdList.DefaultIfEmpty()
                        join ac1 in _dbContext.Accounts on c.CreatedBy equals ac1.AccId into acList1
                        from ac1 in acList1.DefaultIfEmpty()
                        join ac2 in _dbContext.Accounts on c.ModifiedBy equals ac2.AccId into acList2
                        from ac2 in acList2.DefaultIfEmpty()
                        orderby c.CarId
                        select new
                        {
                            Car = c,
                            CarBrand = cb,
                            StatusCategory = cs,
                            GeneralInfoCar = gi,
                            CarDesign = cd,
                            AccountCreatedBy = ac1,
                            AccountModifiedBy = ac2
                        };


            string[] keywords = name.Split("\\s+"); // Tách các từ khóa trong title
            if (!string.IsNullOrEmpty(name))
            {
                for (int i = 0; i < keywords.Length; i++)
                {
                    query = query.Where(x => x.Car.CarName.Contains(name)
                                           || x.CarBrand.BrandName.Contains(name)
                                           || x.CarDesign.Design.Contains(name)
                                        );
                }
            }
            if (brandID != 0)
            {
                query = query.Where(x => x.Car.BrandId == brandID);
            }

            if (designID != 0)
            {
                query = query.Where(x => x.CarDesign.DesignId == designID);
            }

            if (statusID != 0)
            {
                query = query.Where(x => x.StatusCategory.StatusId == statusID);
            }
            
            query =query.Where(x=>x.Car.Price>=minPrice &&  x.Car.Price<=maxPrice).Skip((index - 1) * quantityShow).Take(quantityShow);
            List<CarStaffPage> ls = new List<CarStaffPage>();
            foreach(var z in query)
            {
                CarBrand cb=new CarBrand();
                cb.BrandId = z.CarBrand.BrandId;
                cb.BrandName = z.CarBrand.BrandName;

                StatusCategory sc =new StatusCategory();
                sc.StatusId = z.StatusCategory.StatusId;
                sc.StatusName = z.StatusCategory.StatusName;

                CarDesign cd =new CarDesign();
                cd.DesignId = z.CarDesign.DesignId;
                cd.Design = z.CarDesign.Design;

                CarStaffPage csp = new CarStaffPage(z.Car.CarId, z.Car.CarName, z.Car.Price, cb, sc, cd, z.AccountCreatedBy, z.Car.CreatedOn, z.AccountModifiedBy, z.Car.ModifiedOn);
                ls.Add(csp);
            }
            return ls;
        }

        public int GetTotalCarAdmin(string name, int brandID, int designID, int statusID, double minPrice, double maxPrice)
        {
            var query = from c in _dbContext.Cars
                        join cb in _dbContext.CarBrands on c.BrandId equals cb.BrandId into cbList
                        from cb in cbList.DefaultIfEmpty()
                        join cs in _dbContext.StatusCategories on c.StatusId equals cs.StatusId into csList
                        from cs in csList.DefaultIfEmpty()
                        join gi in _dbContext.GeneralInfoCars on c.CarId equals gi.CarId into giList
                        from gi in giList.DefaultIfEmpty()
                        join cd in _dbContext.CarDesigns on gi.DesignId equals cd.DesignId into cdList
                        from cd in cdList.DefaultIfEmpty()
                        orderby c.CarId
                        select new
                        {
                            Car = c,
                            CarBrand = cb,
                            StatusCategory = cs,
                            GeneralInfoCar = gi,
                            CarDesign = cd
                        };

            string[] keywords = name.Split("\\s+"); // Tách các từ khóa trong title
            if (!string.IsNullOrEmpty(name))
            {
                for (int i = 0; i < keywords.Length; i++)
                {
                    query = query.Where(x => x.Car.CarName.Contains(name)
                                           || x.CarBrand.BrandName.Contains(name)
                                           || x.CarDesign.Design.Contains(name)
                                        );
                }
            }
            if (brandID != 0)
            {
                query = query.Where(x => x.Car.BrandId == brandID);
            }

            if (designID != 0)
            {
                query = query.Where(x => x.CarDesign.DesignId == designID);
            }

            if (statusID != 0)
            {
                query = query.Where(x => x.StatusCategory.StatusId == statusID);
            }

            query = query.Where(x => x.Car.Price >= minPrice && x.Car.Price <= maxPrice);
            List<CarStaffPage> ls = new List<CarStaffPage>();
            //foreach (var z in query)
            //{
            //    CarBrand cb = new CarBrand();
            //    cb.BrandId = z.CarBrand.BrandId;
            //    cb.BrandName = z.CarBrand.BrandName;

            //    StatusCategory sc = new StatusCategory();
            //    sc.StatusId = z.StatusCategory.StatusId;
            //    sc.StatusName = z.StatusCategory.StatusName;

            //    CarDesign cd = new CarDesign();
            //    cd.DesignId = z.CarDesign.DesignId;
            //    cd.Design = z.CarDesign.Design;

            //    Account? accountCreate = _dbContext.Accounts.Where(x => x.AccId == z.Car.CreatedBy).FirstOrDefault();
            //    Account? accountModified = _dbContext.Accounts.Where(x => x.AccId == z.Car.ModifiedBy).FirstOrDefault();
            //    CarStaffPage csp = new CarStaffPage(z.Car.CarId, z.Car.CarName, z.Car.Price, cb, sc, cd, accountCreate, z.Car.CreatedOn, accountModified, z.Car.ModifiedOn);
            //    ls.Add(csp);
            //}
            return query.Count();
        }

        public void UpdateCar(string carName, string price, string statusID, int carID, int modifiedBy)
        {
            try
            {
                Car car=_dbContext.Cars.Where(x=>x.CarId == carID).FirstOrDefault();
                car.CarName = carName;
                car.Price = double.Parse(price);
                car.StatusId = int.Parse(statusID);
                car.ModifiedBy = modifiedBy;
                car.ModifiedOn = DateTime.Now;
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

        public int AddCar(string carName, string price, string brandID, string statusID, int CreateBy)
        {
            Car c = new Car
            {
                CarName = carName,
                Price = double.Parse(price),
                BrandId = int.Parse(brandID),
                StatusId = int.Parse(statusID),
                CreatedBy = CreateBy,
                CreatedOn = DateTime.Now
            };

            _dbContext.Cars.Add(c);
            _dbContext.SaveChanges();
            return c.CarId;
        }

        public void AddEngine(int carID, string fuelTankCapacity, string engineType, string numberOfCylinder, string cylinderCapacity, string variableValveSystem, string fuelSystem, string maximumCapacity, string maximumTorque, string gear)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Insert into EngineAndChassis (carID, fuelTankCapacity, engineType, numberOfCylinder, cylinderCapacity, variableValveSystem, fuelSystem, maximumCapacity, maximumTorque, gear) values (@carID, @fuelTankCapacity, @engineType, @numberOfCylinder, @cylinderCapacity, @variableValveSystem, @fuelSystem, @maximumCapacity, @maximumTorque, @gear)";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carID);
                    com.Parameters.AddWithValue(@"fuelTankCapacity", fuelTankCapacity);
                    com.Parameters.AddWithValue(@"engineType", engineType);
                    com.Parameters.AddWithValue(@"numberOfCylinder", numberOfCylinder);
                    com.Parameters.AddWithValue(@"cylinderCapacity", cylinderCapacity);
                    com.Parameters.AddWithValue(@"variableValveSystem", variableValveSystem);
                    com.Parameters.AddWithValue(@"fuelSystem", fuelSystem);
                    com.Parameters.AddWithValue(@"maximumCapacity", maximumCapacity);
                    com.Parameters.AddWithValue(@"maximumTorque", maximumTorque);
                    com.Parameters.AddWithValue(@"gear", gear);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void AddGeneralInfoCar(int carID, string numberOfSeat, string designID, string madeIn, string fuel, string description)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Insert into GeneralInfoCar (carID, numberOfSeat, designID, madeIn, fuel, description) values (@carID, @numberOfSeat, @designID, @madeIn, @fuel, @description)";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carID);
                    com.Parameters.AddWithValue(@"numberOfSeat", numberOfSeat);
                    com.Parameters.AddWithValue(@"designID", designID);
                    com.Parameters.AddWithValue(@"madeIn", madeIn);
                    com.Parameters.AddWithValue(@"fuel", fuel);
                    com.Parameters.AddWithValue(@"description", description);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void AddImage(int carID, string carIMG)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Insert into CarIMG (carID, carIMG) values (@carID, @carIMG)";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carID);
                    com.Parameters.AddWithValue(@"carIMG", carIMG);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void AddSubImage(int carID, string carSubIMG)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Insert into CarSubIMG (carID,carSubIMGTypeID, carSubIMG) values (@carID,@carSubIMGTypeID, @carSubIMG)";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carID);
                    com.Parameters.AddWithValue("@carSubIMGTypeID", 1);
                    com.Parameters.AddWithValue(@"carSubIMG", carSubIMG);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public CarStaffPage GetCarByID(int carid)
        {
            var query = from c in _dbContext.Cars
                        join cb1 in _dbContext.CarBrands on c.BrandId equals cb1.BrandId into cbList
                        from cb1 in cbList.DefaultIfEmpty()
                        join cs in _dbContext.StatusCategories on c.StatusId equals cs.StatusId into csList
                        from cs in csList.DefaultIfEmpty()
                        join gi in _dbContext.GeneralInfoCars on c.CarId equals gi.CarId into giList
                        from gi in giList.DefaultIfEmpty()
                        join cd1 in _dbContext.CarDesigns on gi.DesignId equals cd1.DesignId into cdList
                        from cd1 in cdList.DefaultIfEmpty()
                        join ac1 in _dbContext.Accounts on c.CreatedBy equals ac1.AccId into acList1
                        from ac1 in acList1.DefaultIfEmpty()
                        join ac2 in _dbContext.Accounts on c.ModifiedBy equals ac2.AccId into acList2
                        from ac2 in acList2.DefaultIfEmpty()
                        orderby c.CarId
                        select new
                        {
                            Car = c,
                            CarBrand = cb1,
                            StatusCategory = cs,
                            GeneralInfoCar = gi,
                            CarDesign = cd1,
                            AccountCreatedBy = ac1,
                            AccountModifiedBy = ac2
                        };
            var z = query.Where(x => x.Car.CarId == carid).FirstOrDefault();
            CarBrand cb = new CarBrand();
            cb.BrandId = z.CarBrand.BrandId;
            cb.BrandName = z.CarBrand.BrandName;

            StatusCategory sc = new StatusCategory();
            sc.StatusId = z.StatusCategory.StatusId;
            sc.StatusName = z.StatusCategory.StatusName;

            CarDesign cd = new CarDesign();
            cd.DesignId = z.CarDesign.DesignId;
            cd.Design = z.CarDesign.Design;

            CarStaffPage csp = new CarStaffPage(z.Car.CarId, z.Car.CarName, z.Car.Price, cb, sc, cd, z.AccountCreatedBy, z.Car.CreatedOn, z.AccountModifiedBy, z.Car.ModifiedOn);
            return csp;
        }

        public void UpdateCar(string carName, string price, string brandID, string statusID, int carID, int modifiedBy)
        {
            try
            {
                Car car = _dbContext.Cars.Where(x => x.CarId == carID).FirstOrDefault();
                car.CarName = carName;
                car.BrandId = int.Parse(brandID);
                car.Price = double.Parse(price);
                car.StatusId = int.Parse(statusID);
                car.ModifiedBy = modifiedBy;
                car.ModifiedOn = DateTime.Now;
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
            }
        }

        public void UpdateEngine(string carId, string capacity, string engineType, string cylinderNumber, string cylinderMaxCapacity, string variableVale, string fuelSystem, string fuelCapacity, string maxTorque, string gear)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Update EngineAndChassis set fuelTankCapacity=@fuelTankCapacity, engineType=@engineType, numberOfCylinder=@numberOfCylinder, cylinderCapacity=@cylinderCapacity, variableValveSystem=@variableValveSystem, fuelSystem=@fuelSystem, maximumCapacity=@maximumCapacity, maximumTorque=@maximumTorque, gear=@gear WHERE carId=@carid";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carId);
                    com.Parameters.AddWithValue(@"fuelTankCapacity", double.Parse(capacity));
                    com.Parameters.AddWithValue(@"engineType", engineType);
                    com.Parameters.AddWithValue(@"numberOfCylinder", cylinderNumber);
                    com.Parameters.AddWithValue(@"cylinderCapacity", double.Parse(cylinderMaxCapacity));
                    com.Parameters.AddWithValue(@"variableValveSystem", variableVale);
                    com.Parameters.AddWithValue(@"fuelSystem", fuelSystem);
                    com.Parameters.AddWithValue(@"maximumCapacity", fuelCapacity);
                    com.Parameters.AddWithValue(@"maximumTorque", maxTorque);
                    com.Parameters.AddWithValue(@"gear", gear);
                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void UpdateGeneralInfoCar(string carId, string seatNumber, string designID, string madeIn, string fuel, string description)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Update GeneralInfoCar SET  numberOfSeat = @numberOfSeat, designID=@designID, madeIn=@madeIn, fuel=@fuel, description=@description WHERE carID=@carID";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carId);
                    com.Parameters.AddWithValue(@"numberOfSeat", seatNumber);
                    com.Parameters.AddWithValue(@"designID", designID);
                    com.Parameters.AddWithValue(@"madeIn", madeIn);
                    com.Parameters.AddWithValue(@"fuel", fuel);
                    com.Parameters.AddWithValue(@"description", description);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void UpdateImage(string carId, string newNameImage)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Update CarIMG set carIMG=@carIMG where carId=@carId";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carId);
                    com.Parameters.AddWithValue(@"carIMG", newNameImage);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        public void UpdateSubImage(string carId, string newSubNameImage)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(getConnectionString()))
                {
                    string sql = @"Update CarSubIMG set carSubIMG=@carSubIMG where carId=@carId";
                    con.Open();
                    SqlCommand com = new SqlCommand(sql, con);

                    com.Parameters.AddWithValue("@carID", carId);
                    com.Parameters.AddWithValue(@"carSubIMG", newSubNameImage);

                    com.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
            }
        }
        public List<CarBrand> GetCarBrands()
        {
            return _dbContext.CarBrands.ToList();
        }
        public List<Car> GetAllCars()
        {
            return _dbContext.Cars.ToList();
        }
        public Car GetCarById(int id)
        {
            return _dbContext.Cars.FirstOrDefault(x => x.CarId == id);
        }
        public EngineAndChassi GetEngineByCarId(int id)
        {
            return _dbContext.EngineAndChasses.FirstOrDefault(x => x.CarId == id);
        }
        public GeneralInfoCar GetGeneralInfoCarByCarId(int id)
        {
            return _dbContext.GeneralInfoCars.FirstOrDefault(x => x.CarId == id);
        }
        public List<Car> GetListCarCE(int brandID, int designID)
        {
            var cars = _dbContext.Cars
                        .GroupJoin(
                            _dbContext.GeneralInfoCars,
                            car => car.CarId,
                            gi => gi.CarId,
                            (car, gis) => new { car, gis }
                        )
                        .SelectMany(
                            temp => temp.gis.DefaultIfEmpty(),
                            (temp, gi) => new { temp.car, gi }
                        )
                        .GroupJoin(
                            _dbContext.CarDesigns,
                            joined => joined.gi != null ? joined.gi.DesignId : 0,
                            cd => cd.DesignId,
                            (joined, cds) => new { joined.car, joined.gi, cds }
                        )
                        .SelectMany(
                            temp => temp.cds.DefaultIfEmpty(),
                            (temp, cd) => new { temp.car, temp.gi, cd }
                        )
                        .Where(joined => joined.car.StatusId == 1 && joined.car.BrandId == brandID && (joined.cd == null || joined.cd.DesignId == designID))
                        .Select(joined => new Car
                        {
                            CarId = joined.car.CarId,
                            CarName = joined.car.CarName,
                            Price = joined.car.Price,
                        })
                        .ToList();

            return cars;
        }
        public double GetCarPrice(int carID)
        {
            Car car = _dbContext.Cars.FirstOrDefault(c => c.CarId == carID);
            return car.Price;
        }
        public CarSubImg GetCarSubImg(int carID)
        {
            return _dbContext.CarSubImgs.FirstOrDefault(c => carID == c.CarId);
        }
        public void AddOrder(String carorderCode, int clientID, int carID, bool paymentType, long orderValue, long paid, long paymentRequired, DateTime createdOn, DateTime orderDate, String status)
        {
            CarOrder order = new CarOrder
            {
                CarorderCode = carorderCode,
                ClientId = clientID,
                CarId = carID,
                PaymentType = paymentType,
                OrderValue = orderValue,
                Paid = paid,
                PaymentRequired = paymentRequired,
                CreatedOn = createdOn,
                OrderDate = orderDate,
                Status = status,
            };
            _dbContext.CarOrders.Add(order);
            _dbContext.SaveChanges();
        }
        public CarBrand GetBrandById(int Id) 
        {
            return _dbContext.CarBrands.FirstOrDefault(x => x.BrandId == Id);
        }
        public CarDesign GetDesignById(int Id)
        {
            return _dbContext.CarDesigns.FirstOrDefault(x => x.DesignId == Id);
        }
    }
}
