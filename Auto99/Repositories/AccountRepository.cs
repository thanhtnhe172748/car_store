﻿using Auto99.Models;
using Microsoft.EntityFrameworkCore;

namespace Auto99.Repositories
{
    public interface IAccountRepository
    {
        ClientAccount GetAccount(String email, String pass);
        void AddAccount(ClientAccount account);
        bool CheckAccountExist(string email);
        void AddAccountProfile(Client accPro);
        int GetClientIdByEmail(string email);
        ClientAccount GetClientAccountById(int id);
        void UpdateUser(ClientAccount user);
    }
    public class AccountRepository : IAccountRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public AccountRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }
        public ClientAccount GetAccount(String email, String pass)
        {

            return _dbContext.ClientAccounts.FirstOrDefault(a => a.Email == email && a.Pass == pass);
        }
        public void AddAccount(ClientAccount account)
        {
            _dbContext.ClientAccounts.Add(account);
            _dbContext.SaveChanges();
        }
        public bool CheckAccountExist(string  email)
        {
            if (_dbContext.ClientAccounts.FirstOrDefault(x => x.Email.Equals(email)) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public void AddAccountProfile(Client accPro)
        {
            _dbContext.Clients.Add(accPro);
            _dbContext.SaveChanges();
        }
        public int GetClientIdByEmail(string email)
        {
            return _dbContext.Clients.Where(x => x.Email.Equals(email)).Select(x => x.ClientId).FirstOrDefault();
        }
        public ClientAccount GetClientAccountById(int id)
        {
            var clientAccount = _dbContext.ClientAccounts.FirstOrDefault(x => x.AccId == id);
            if (clientAccount == null)
            {
                Console.WriteLine($"ClientAccount with AccId {id} not found.");
            }
            return clientAccount;
        }
        public void UpdateUser(ClientAccount user)
        {
            _dbContext.ClientAccounts.Update(user);
            _dbContext.SaveChanges();
        }
    }
}
