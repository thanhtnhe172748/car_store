﻿using Auto99.Message;
using Auto99.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using NuGet.Packaging.Signing;
using System.Configuration;
using System.Drawing.Printing;
using System.Text.RegularExpressions;

namespace Auto99.Repositories
{
    public interface ILocationRepository
    {
        List<Location> getListLocation();
        List<Location> getListLocation(int index, int quantity, string locationName);
        Location getLocationByID(int locationID);
        int getTotalLocation(string locationName);
        void addLocation(string locationName, string preRegFee, string regFee, int createdBy);
        void updateLocation(string locationName, string preRegFee, string regFee, int modifiedBy, int locationID);
        void deleteLocation(int locationID);
    }
    public class LocationRepository : ILocationRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public LocationRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }

        public List<Location> getListLocation()
        {
            var query = _dbContext.Locations.Select(x=>x);
            return query.ToList();
        }

        public List<Location> getListLocation(int index, int quantity, string locationName)
        {
            //List<Location> list = new ArrayList<>();
            List<Location> query = (from lo in _dbContext.Locations
                        orderby lo.LocationId
                        select lo).ToList();
            if (!string.IsNullOrEmpty(locationName))
            {
                query = query.Where(x => x.LocationName.Contains(locationName)).ToList();
            }

                        
           return query.Skip((index - 1) * quantity).Take(quantity).ToList();
        }

        public Location getLocationByID(int locationID)
        {
            Location query =_dbContext.Locations.Where(x=>x.LocationId.Equals(locationID)).FirstOrDefault();
            
            return query;
        }

        public int getTotalLocation(string locationName)
        {
            var query = _dbContext.Locations.Where(x => x.LocationName.Contains(locationName));
            return query.Count();
        }

        public void addLocation(string locationName, string preRegFee, string regFee, int createdBy)
        {

            Location lo = new Location();
            lo.LocationName = locationName;
            lo.PreRegFee = double.Parse(preRegFee);
            lo.RegFee= double.Parse(regFee);
            lo.CreatedBy= createdBy;
            lo.CreatedOn = DateTime.Now;

            _dbContext.Locations.Add(lo);
            _dbContext.SaveChanges();
        }

        public void updateLocation(string locationName, string preRegFee, string regFee, int modifiedBy, int locationID)
        {
            Location lo = _dbContext.Locations.Where(x => x.LocationId.Equals(locationID)).FirstOrDefault();
            lo.LocationName = locationName;
            lo.PreRegFee = double.Parse(preRegFee);
            lo.RegFee = double.Parse(regFee);
            lo.ModifiedBy = modifiedBy;
            lo.ModifiedOn = DateTime.Now;

            _dbContext.SaveChanges();
        }

        public void deleteLocation( int locationID)
        {
            Location lo = _dbContext.Locations.Where(x => x.LocationId.Equals(locationID)).FirstOrDefault();
            _dbContext.Locations.Remove(lo);
            _dbContext.SaveChanges();
        }
    }

}

