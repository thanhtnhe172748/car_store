﻿using Auto99.Message;
using Auto99.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileSystemGlobbing.Internal;
using NuGet.Packaging.Signing;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;

namespace Auto99.Repositories
{
    public interface IEmployeeRepository
    {
        List<EmployeeProfile> GetListEmployee(string action, string filter, string search, int pageSize, int quantityShow);
        List<EmployeeProfile> GetListEmployee();
        List<Role> GetListRole();
        int GetTotalEmployee(string action, string filter, string search);
        bool UpdateEmployeeById(int employeeID, string firstName, string lastName, string email, string phoneNumber, string DOB, string img, int gender, string IDNo, string address, string startDate, int modifiedBy, int roleId);
        bool InsertEmployee(string firstName, string lastName, string email, string phoneNumber, string DOB, string img, int gender, string IDNo, string address, string startDate, int createdBy, string accountName, string password, int roleId);
        string CheckInsertEmployee(string accountName, string email, string phoneNumber, string idNo);
        EmployeeProfile getEmployeeById(int? id);
    }
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly database_SWP391_Auto99_version35Context _dbContext;

        public EmployeeRepository(database_SWP391_Auto99_version35Context dbContext)
        {
            _dbContext = dbContext;
        }
        public List<EmployeeProfile> GetListEmployee()
        {
            var query = from ep in _dbContext.EmployeeProfiles
                        join a in _dbContext.Accounts on ep.EmployeeId equals a.AccId
                        join r in _dbContext.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName),
                            Employee =a
                        };

            
            return query.ToList();
        }

        public List<EmployeeProfile> GetListEmployee(string action, string filter, string search, int pageSize, int quantityShow)
        {
            var query = from ep in _dbContext.EmployeeProfiles
                        join a in _dbContext.Accounts on ep.EmployeeId equals a.AccId
                        join r in _dbContext.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName)
                        };
            
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Equals("1") ? "True" : "False";
                query = query.Where(x => x.Gender == bool.Parse(filter));
            }
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x =>
                    x.FirstName.Contains(search) ||
                    x.LastName.Contains(search) ||
                    x.Email.Contains(search)
                //||    (x.FirstName + "+' '+" + x.LastName).Contains(search)
                //|| x.Dob.Value.Equals(search) // nếu search là ngày tháng, hãy đảm bảo bạn có định dạng đúng
                //|| x.PhoneNumber.Contains(search)
                //|| x.Idno.Contains(search)
                //|| x.Address.Contains(search)
                );
            }
            return query.Skip((pageSize - 1) * quantityShow).Take(quantityShow).ToList();
        }

        public int GetTotalEmployee(string action, string filter, string search)
        {
            var query = from ep in _dbContext.EmployeeProfiles
                        join a in _dbContext.Accounts on ep.EmployeeId equals a.AccId
                        join r in _dbContext.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName)
                        };

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.Equals("1") ? "True" : "False";
                query = query.Where(x => x.Gender == bool.Parse(filter));
            }
            if (!string.IsNullOrEmpty(search))
            {
                query = query.Where(x => x.FirstName.Contains(search)
                || x.LastName.Contains(search)
                || x.Email.Contains(search)
                //|| x.Dob.Value.Equals(text)
                //|| x.PhoneNumber.Contains(text)
                //|| x.Idno.Contains(text)
                //|| x.Address.Contains(text)
                );
            }
            return query.Count();
        }

        public List<Role> GetListRole()
        {
            var query = from r in _dbContext.Roles
                        select new Role
                        {
                            RoleId = r.RoleId,
                            RoleName = r.RoleName,
                        };
            return query.ToList();
        }

        public bool UpdateEmployeeById(int employeeID, string firstName, string lastName, string email, string phoneNumber, string DOB, string img, int gender, string IDNo, string address, string startDate, int modifiedBy, int roleId)
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                                    .AddJsonFile("appsettings.json")
                                    .Build();

                // Retrieve the connection string
                string connectionString = configuration.GetConnectionString("DbString");
                string sql = @"UPDATE EmployeeProfile
                       SET firstName = @FirstName,
                           lastName = @LastName,
                           email = @Email,
                           DOB = @DOB,
                           phoneNumber = @PhoneNumber,
                           img = @Img,
                           gender = @Gender,
                           IDNo = @IDNo,
                           [address] = @Address,
                           startDate = @StartDate,
                           modifiedBy = @ModifiedBy,
                           modifiedOn = @ModifiedOn
                       WHERE employeeID = @EmployeeID";

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(sql, connection);

                    // Add parameters
                    command.Parameters.AddWithValue("@FirstName", firstName);
                    command.Parameters.AddWithValue("@LastName", lastName);
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@DOB", DOB);
                    command.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                    command.Parameters.AddWithValue("@Img", img); // or the image data
                    command.Parameters.AddWithValue("@Gender", gender);
                    command.Parameters.AddWithValue("@IDNo", IDNo);
                    command.Parameters.AddWithValue("@Address", address);
                    command.Parameters.AddWithValue("@StartDate", startDate);
                    command.Parameters.AddWithValue("@ModifiedBy", modifiedBy);
                    command.Parameters.AddWithValue("@ModifiedOn", DateTime.Now);
                    command.Parameters.AddWithValue("@EmployeeID", employeeID); // replace with the actual employee ID

                    connection.Open();
                    int rowsAffected = command.ExecuteNonQuery();
                    Console.WriteLine($"{rowsAffected} row(s) updated.");
                }

                Account a=_dbContext.Accounts.FirstOrDefault(x => x.AccId == employeeID);
                    a.RoleId = roleId;
                    _dbContext.SaveChanges();

                    return true;
            }
            catch (Exception ex)
            {
                // Log the exception or handle it appropriately
                Console.WriteLine($"Error: {ex.Message}");
                return false;
            }

        }
        public bool InsertEmployee(string firstName, string lastName, string email, string phoneNumber, string DOB, string img, int gender, string IDNo, string address, string startDate, int createdBy, string accountName, string password, int roleId)
        {
            try
            {
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();
                    int id = _dbContext.EmployeeProfiles.Max(x => x.EmployeeId) + 1;

                    string connectionString = configuration.GetConnectionString("DbString");
                    string sql = @"SET IDENTITY_INSERT [Account] ON;" +
                    "INSERT INTO [dbo].[Account]([accID],[accName],[password],[roleID],[email],[status]) VALUES(@accID,@accName,@password,@roleID,@email,@status)" +
                    "SET IDENTITY_INSERT [Account] OFF;";

                    string sql1 = @"SET IDENTITY_INSERT [EmployeeProfile] ON;"+
                    "INSERT INTO [dbo].[EmployeeProfile]([employeeID],[firstName],[lastName],[email],[DOB],[phoneNumber],[img],[gender],[IDNo],[address],[startDate],[createdBy],[createdOn])" +
                    "VALUES(@EmpID,@FirstName,@LastName,@Email,@DOB,@PhoneNumber,@Img,@Gender,@IDNo,@Address,@StartDate,@CreatedBy,@CreatedOn)"+
                    "SET IDENTITY_INSERT [EmployeeProfile] OFF;";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(sql1, connection);
                    SqlCommand command1 = new SqlCommand(sql, connection);
                    // Add parameters Account
                    command1.Parameters.AddWithValue("@accID", id);
                    command1.Parameters.AddWithValue("@accName", accountName);
                    command1.Parameters.AddWithValue("@password", password);
                    command1.Parameters.AddWithValue("@roleID", roleId);
                    command1.Parameters.AddWithValue("@email", email);
                    command1.Parameters.AddWithValue("@status", false);
                    // Add parameters Employee
                    command.Parameters.AddWithValue("@EmpID", id);
                    command.Parameters.AddWithValue("@FirstName", firstName);
                    command.Parameters.AddWithValue("@LastName", lastName);
                    command.Parameters.AddWithValue("@Email", email);
                    command.Parameters.AddWithValue("@DOB", DOB);
                    command.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                    command.Parameters.AddWithValue("@Img", img); // or the image data
                    command.Parameters.AddWithValue("@Gender", gender);
                    command.Parameters.AddWithValue("@IDNo", IDNo);
                    command.Parameters.AddWithValue("@Address", address);
                    command.Parameters.AddWithValue("@StartDate", startDate);
                    command.Parameters.AddWithValue("@CreatedBy", createdBy);
                    command.Parameters.AddWithValue("@CreatedOn", DateTime.Now);

                    connection.Open();
                    command1.ExecuteNonQuery();
                    int rowsAffected = command.ExecuteNonQuery();
                }
                    _dbContext.SaveChanges();
                    return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public string CheckInsertEmployee(string accountName, string email, string phoneNumber, string idNo)
        {
            ConstantMessages constanMessage = new ConstantMessages();
            if (accountName == null || email == null || phoneNumber == null || idNo == null)
            {
                return constanMessage.EMPTY;
            }
            //check pattern email
            string emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
                    + "[a-zA-Z0-9_+&*-]+)*@"
                    + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
                    + "A-Z]{2,7}$";

            if (!Regex.IsMatch(email, emailRegex))
            {
                return constanMessage.PATTERN +" Email.";
            }

            //check pattern phoneNumber
            string phoneNumberRegex = @"^0\d{9}$";
            if (!Regex.IsMatch(phoneNumber, phoneNumberRegex))
            {
                return constanMessage.PATTERN + " số điện thoại.";
            }

            //check pattern CMND
            if (idNo.Length != 12 || !Regex.IsMatch(idNo, @"^\d{12}$"))
            {
                return constanMessage.PATTERN + " CMND.";
            }

            //check duplicate
            try
            {
                //check account
                var checkAccount = _dbContext.Accounts.Where(x=>x.AccName.Equals(accountName)).FirstOrDefault();
                if (checkAccount != null)
                {
                    return constanMessage.DUPALICATEACCOUNT;
                }

                var checkEmail = _dbContext.Accounts.Where(x => x.Email.Equals(email)).FirstOrDefault();
                if (checkEmail != null)
                {
                    return constanMessage.DUPALICATEEMAIL;
                }

                var checkPhoneNumber = _dbContext.EmployeeProfiles.Where(x => x.PhoneNumber.Equals(phoneNumber)).FirstOrDefault();
                if (checkPhoneNumber != null)
                {
                    return constanMessage.DUPALICATEPHONE;
                }

                var checkIdNo = _dbContext.EmployeeProfiles.Where(x => x.Idno.Equals(idNo)).FirstOrDefault();
                if (checkIdNo != null)
                {
                    return constanMessage.DUPALICATEIDNO;
                }
            }
            catch (Exception e)
            {
            }
            return constanMessage.SUCCESS;
        }

        public EmployeeProfile getEmployeeById(int? id)
        {
            var query = from ep in _dbContext.EmployeeProfiles
                        join a in _dbContext.Accounts on ep.EmployeeId equals a.AccId
                        join r in _dbContext.Roles on a.RoleId equals r.RoleId
                        orderby ep.EmployeeId
                        select new EmployeeProfile
                        {
                            EmployeeId = ep.EmployeeId,
                            FirstName = ep.FirstName,
                            LastName = ep.LastName,
                            Email = ep.Email,
                            PhoneNumber = ep.PhoneNumber,
                            Dob = ep.Dob,
                            Img = ep.Img,
                            Gender = ep.Gender,
                            Idno = ep.Idno,
                            Address = ep.Address,
                            StartDate = ep.StartDate,
                            Role = new Role(r.RoleId, r.RoleName),
                            Employee = a
                        };
            return query.Where(x => x.EmployeeId == id).FirstOrDefault();
        }
    }

}

