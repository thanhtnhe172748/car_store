﻿using System;
using System.Collections.Generic;

namespace Auto99.Models
{
    public partial class CarSubImgtype
    {
        public int CarSubImgtypeId { get; set; }
        public string? CarSubImgtype1 { get; set; }
    }
}
