﻿using NuGet.Packaging.Signing;

namespace Auto99.Models
{
    public class CarStaffPage
    {
        public int carID;
        public string? carName;
        public double? price;
        public CarBrand? brandID;
        public StatusCategory? status;
        public CarDesign? design;
        public CarImg? carIMG;
        public CarSubImg? carSubIMG;
        public string? madeIn;
        public int? numberOfSeat;
        public string? fuel;
        public string? engineType;
        public string? gear;
        public float? cylinderCapacity;
        public string? description;
        public Account? createdBy;
        public DateTime? createdOn;
        public Account? modifiedBy;
        public DateTime? modifiedOn;

        public CarStaffPage(int carID, string?carName, double? price, CarBrand? brandID, StatusCategory? status, CarDesign? design, Account? createdBy, DateTime? createdOn, Account? modifiedBy, DateTime? modifiedOn)
        {
            this.carID = carID;
            this.carName = carName;
            this.price = price;
            this.brandID = brandID;
            this.status = status;
            this.design = design;
            this.createdBy = createdBy;
            this.createdOn = createdOn;
            this.modifiedBy = modifiedBy;
            this.modifiedOn = modifiedOn;
        }

        public CarStaffPage(int carID, string carName, double price, CarBrand brandID, StatusCategory status, CarDesign design, CarImg carIMG, CarSubImg carSubIMG, string madeIn, int numberOfSeat, string fuel, string engineType, string gear, float cylinderCapacity)
        {
            this.carID = carID;
            this.carName = carName;
            this.price = price;
            this.brandID = brandID;
            this.status = status;
            this.design = design;
            this.carIMG = carIMG;
            this.carSubIMG = carSubIMG;
            this.madeIn = madeIn;
            this.numberOfSeat = numberOfSeat;
            this.fuel = fuel;
            this.engineType = engineType;
            this.gear = gear;
            this.cylinderCapacity = cylinderCapacity;
        }

        public CarStaffPage(int carID, string carName, double price, CarBrand brandID, StatusCategory status, CarDesign design, CarImg carIMG, CarSubImg carSubIMG, string madeIn, int numberOfSeat, string fuel, string description, string engineType, string gear, float cylinderCapacity)
        {
            this.carID = carID;
            this.carName = carName;
            this.price = price;
            this.brandID = brandID;
            this.status = status;
            this.design = design;
            this.carIMG = carIMG;
            this.carSubIMG = carSubIMG;
            this.madeIn = madeIn;
            this.numberOfSeat = numberOfSeat;
            this.fuel = fuel;
            this.description = description;
            this.engineType = engineType;
            this.gear = gear;
            this.cylinderCapacity = cylinderCapacity;
        }

        public CarStaffPage(int carID, string carName, double price, CarBrand brandID, StatusCategory status, CarDesign design, Account createdBy, DateTime createdOn, Account modifiedBy, DateTime modifiedOn)
        {
            this.carID = carID;
            this.carName = carName;
            this.price = price;
            this.brandID = brandID;
            this.status = status;
            this.design = design;
            this.createdBy = createdBy;
            this.createdOn = createdOn;
            this.modifiedBy = modifiedBy;
            this.modifiedOn = modifiedOn;
        }
    }
}
